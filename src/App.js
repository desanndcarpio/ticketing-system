import {useState, useEffect} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';

import './App.css';

import {UserProvider} from './UserContext';

import AppNavbar from './components/AppNavbar'
import Sidebar from './components/Sidebar'

import ADashboard from './pages/ADashboard'
import AllBTickets from './pages/AllBTickets'
import AllSTickets from './pages/AllSTickets'
import BApproval from './pages/BApproval'
import BCancelled from './pages/BCancelled'
import BForApproval from './pages/BForApproval'
import BInProgress from './pages/BInProgress'
import BPending from './pages/BPending'
import BRefund from './pages/BRefund'
import BReqRefund from './pages/BReqRefund'
import BSuccess from './pages/BSuccess'
import BTicketView from './pages/BTicketView'
import BuyerTicket from './pages/BuyerTicket'
import Cash from './pages/Cash'
import ClientProfile from './pages/ClientProfile'
import Home from './pages/Home'
import IDashboard from './pages/IDashboard'
import Login from './pages/Login'
import Logout from './pages/Logout'
import MDashboard from './pages/MDashboard'
import MyProfile from './pages/MyProfile'
import NotFound from './pages/NotFound'
import OnlinePayment from './pages/OnlinePayment'
import OrderSummary from './pages/OrderSummary'
import Register from './pages/Register'
import ReqRefundTicket from './pages/ReqRefundTicket'
import SAddCredits from './pages/SAddCredits'
import SBDashboard from './pages/SBDashboard'
import SDBError from './pages/SDBError'
import SDeclined from './pages/SDeclined'
import SITickets from './pages/SITickets'
import SMaintenance from './pages/SMaintenance'
import SOthers from './pages/SOthers'
import SResolved from './pages/SResolved'
import StaffTicket from './pages/StaffTicket'
import STicketView from './pages/STicketView'
import SUnresolved from './pages/SUnresolved'
import SUrgent from './pages/SUrgent'
import Users from './pages/Users'


function App() {

  const [user, setUser] = useState({
    id: null,
    role: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }


  useEffect(() => {

    let token = localStorage.getItem('token'); 

    fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
      method: 'GET',
      headers: {
        Authorization : `Bearer ${token}`
      }
    })
    .then(res=> res.json())
    .then(data => {
      //console.log(data)

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          role: data.role
        })
      } else {
        setUser ({
          id: null,
          role: null
        })
      }
    })
  }, [])




  

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <Container fluid className= "p-0 m-0 main-container">
        
          <AppNavbar/>
              <Routes>
                <Route exact path ="/" element={<Home/>}/>

                <Route exact path ="/ADashboard" element={<ADashboard/>}/>
                <Route exact path ="/AllBTickets" element={<AllBTickets/>}/>
                <Route exact path ="/AllSTickets" element={<AllSTickets/>}/>
                <Route exact path ="/BApproval" element={<BApproval/>}/>
                <Route exact path ="/BCancelled" element={<BCancelled/>}/>
                <Route exact path ="/BForApproval" element={<BForApproval/>}/>
                <Route exact path ="/BInProgress" element={<BInProgress/>}/>
                <Route exact path ="/BPending" element={<BPending/>}/>
                <Route exact path ="/BRefund" element={<BRefund/>}/>
                <Route exact path ="/BReqRefund" element={<BReqRefund/>}/>
                <Route exact path ="/BSuccess" element={<BSuccess/>}/>
                <Route exact path ="/buyerTickets/:ticketId" element={<BTicketView/>}/>
                <Route exact path ="/buyerTicket" element={<BuyerTicket/>}/>
                <Route exact path ="/Cash" element={<Cash/>}/>
                <Route exact path ="/clientProfile" element={<ClientProfile/>}/>
                <Route exact path ="/IDashboard" element={<IDashboard/>}/>
                <Route exact path ="/login" element={<Login/>}/>
                <Route exact path ="/logout" element={<Logout/>}/>
                <Route exact path ="/MDashboard" element={<MDashboard/>}/>
                <Route exact path ="/MyProfile" element={<MyProfile/>}/>
                <Route exact path ="/OnlinePayment" element={<OnlinePayment/>}/>
                <Route exact path ="/OrderSummary" element={<OrderSummary/>}/>
                <Route exact path ="/register" element={<Register/>}/>
                <Route exact path ="/ReqRefundTicket" element={<ReqRefundTicket/>}/>
                <Route exact path ="/SAddCredits" element={<SAddCredits/>}/>
                <Route exact path ="/sbDashboard" element={<SBDashboard/>}/>
                <Route exact path ="/SDBError" element={<SDBError/>}/>
                <Route exact path ="/SDeclined" element={<SDeclined/>}/>
                <Route exact path ="/SITickets" element={<SITickets/>}/>
                <Route exact path ="/SMaintenance" element={<SMaintenance/>}/>
                <Route exact path ="/SOthers" element={<SOthers/>}/>
                <Route exact path ="/SResolved" element={<SResolved/>}/>
                <Route exact path ="/staffTicket" element={<StaffTicket/>}/>
                <Route exact path ="/staffTickets/:ticketId" element={<STicketView/>}/>
                <Route exact path ="/SUnresolved" element={<SUnresolved/>}/>
                <Route exact path ="/SUrgent" element={<SUrgent/>}/>
                <Route exact path ="/Users" element={<Users/>}/>

                <Route path= "*" element= {<NotFound/>} />
              </Routes>
        </Container>
        <footer className="-footer-pin">
         
        </footer>
      </Router>
    </UserProvider>
  );
}

export default App;

