//import {useState, useEffect, useContext, Fragment} from 'react'
import {Fragment} from 'react'
import {Table, Button} from 'react-bootstrap'
//import {FaTrashAlt} from "react-icons/fa";
//import {Navigate, useNavigate, Link} from 'react-router-dom'
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
//import UserContext from '../UserContext' 

export default function ApprovalCard({BApprovalProp}){

	//const {_id, buyer, isOneWay, origin, destination, airline, dateDeparture, dateArrival, adult, child, infant, passengers, messages, createdOn, isActive, status, approval} = BApprovalProp

	const {_id, buyer, isOneWay, origin, destination, airline, dateDeparture, dateArrival, messages, status, approval} = BApprovalProp
	//console.log(BPendingProp)
	
	const history = useNavigate()

	function renderMessages (messages) {
		return messages.map(message => {
			//console.log(message)
			return (
				<tr>
					<td>{message.sender.name} : {message.message}</td>
				</tr>
			)
		})
	}

	function addMessage(e){
		let {value: newMessage} = Swal.fire({
			title: 'Add Message',
			input: 'text',
			showCancelButton: true,
			preConfirm: (newMessage) => {
				//console.log(newMessage)
				if(newMessage){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/addMessage`, {
						method: 'POST',
						headers: {
							"Content-Type" : "application/json",
							Authorization : `Bearer ${localStorage.getItem("token")}`
						},
						body: JSON.stringify({
							message: newMessage
						})
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						history(0)
					})
				}
			}
		})
	}

	function changeStatus(e){
		const {value: newStatus} = Swal.fire({
			title: 'Select Status',
			input: 'select',
			inputOptions: {
				'inProgress' : 'In Progress',
				'pending' : 'Pending',
				'refund' : 'Refund',
				'success' : 'Success',
				'cancelled' : 'Cancelled'
			},
			inputPlaceholder: 'Select new Status',
			showCancelButton: true,
			inputValidator : (value) => {
				if(value){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/status`, {
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							status: value
						})
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
						history(0)
					})
				}
			}
		})
	}


	return(
		<Fragment>
			<h5>Control Number: {_id}</h5>
			<h5>Customer: {buyer.name}</h5>
			<h6>Email: {buyer.email}</h6>
		<Table striped bordered hover>
			<thead>
				<tr>
					
					<th>  </th>
					<th>Airline</th>
					<th>Origin</th>
					<th>Destination</th>
					<th>Departure Date</th>
					<th>Arrival Date</th>
					<th>Status</th>
					<th>Approval Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{isOneWay ? 'One Way' : 'Round Trip'}</td>
					<td>{airline}</td>
					<td>{origin}</td>
					<td>{destination}</td>
					<td>{dateDeparture}</td>
					<td>{dateArrival}</td>
					<td>{status}</td>
					<td>{approval}</td>

					<td>
						<Button onClick={(e) => addMessage(e)}>Add message</Button>
						<Button onClick={(e) => changeStatus(e)}>Change Status</Button>

					</td>
				</tr>
				{renderMessages(messages)}
			</tbody>
		</Table>
		</Fragment>
	)
}