import {useContext, Fragment, useState} from 'react'
import {Table, Button, Container, Accordion} from 'react-bootstrap'
//import {FaTrashAlt} from "react-icons/fa";
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext' 

export default function UsersCard({UsersProp}){

	const {_id, name, email, role, isActive} = UsersProp

	const history = useNavigate()

	//const [newRole, setNewRole] = useState('');

	//const {user, setUser} = useContext(UserContext)

	//console.log(UsersProp)

	function changeRole(e){

		const {value: newRole} = Swal.fire({
			title: 'Select Role',
			input: 'select',
			inputOptions: {
				'user' : 'User',
				'staff' : 'Staff',
				'it' : 'IT',
				'Manager' : 'Manager',
				'Admin' : 'Admin'
			},
			inputPlaceholder: 'Select new Role',
			showCancelButton:  true,
			inputValidator : (value) => {
				if(value){
					fetch(`https://airline-ticketing-api.onrender.com/users/${_id}/role`, {
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							role: value
						})
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
							Swal.fire({
								title: 'Success',
								icon: 'success',
								text: 'User Role updated'
							})

							history(0)
					})
				}
			}
		})
	}

	function changeStatus(e){

		const {value: newStatus} = Swal.fire({
			title: 'Select Status',
			input: 'select',
			inputOptions: {
				'true' : 'Activate',
				'false' : 'Deactivate'
			},
			inputPlaceholder : 'Select new Status',
			showCancelButton: true,
			inputValidator : (value) => {
				if(value){
					fetch(`https://airline-ticketing-api.onrender.com/users/${_id}/status`, {
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							isActive: value
						})
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
						Swal.fire({
							title: 'Success',
							icon: 'success',
							text: 'User status updated'
						})

						history(0)
					})
				}
			}
		})
	}



	return(
		<Fragment>
				<tr>
						<td>{_id}</td>
						<td>{name}</td>
						<td>{email}</td>
						<td>{role}</td>
						<td>{isActive ? 'Active' : 'Deactivated'}</td>
						<td>
							<Button variant="secondary" className="mx-2" onClick={(e) => changeRole(e)}>Change Role</Button> 
							<Button variant="secondary" className="mx-2" onClick={(e) => changeStatus(e)}>Change Status</Button> 
						</td>
				</tr>
		</Fragment>
	)
}