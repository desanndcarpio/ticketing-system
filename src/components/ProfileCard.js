import {useState, useContext, Fragment} from 'react'
import {Table, Button, Row, Modal, Form, Col} from 'react-bootstrap'
//import {FaTrashAlt} from "react-icons/fa";
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext' 
import '../App.css';

export default function ProfileCard({ViewProfileProp}){

	const {_id, userId, name, email, address, gender, birthday, mobileNo, age, passportNo } = ViewProfileProp

	const [show, setShow] = useState(false);

	const history = useNavigate()

	const [newName, setNewName ] = useState({name});
	const [newBirthday, setNewBirthday] = useState({birthday});
	const [newAge, setNewAge] = useState({age});
	const [newGender, setNewGender] = useState({gender});
	const [newAddress, setNewAddress] = useState('');
	const [newEmail, setNewEmail] = useState('');
	const [newMobileNo, setNewMobileNo] = useState('');
	const [newPassportNo, setNewPassportNo] = useState({passportNo});

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const {user, setUser} = useContext(UserContext)

	const editProfile = (e, profileId) => {
		e.preventDefault();

		fetch(`https://airline-ticketing-api.onrender.com/clientProfiles/${_id}/edit`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: newName,
				email: newEmail,
				mobileNo: newMobileNo,
				address: newAddress,
				gender: newGender,
				birthday: newBirthday,
				age: newAge,
				passportNo: newPassportNo
			})
		})
		.then(res=> res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Passenger Details Updated'
				})

				history(0)
			} else {
				console.log(data)
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Error'
				})
			}
		})
	}

	function confirmDelete(e){
		Swal.fire({
  			title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
		})
		.then((result) => {
			//console.log(result)
			if(result.isConfirmed){
				deleteProfile(result)
			}
		})
	}

	function deleteProfile(e){
		fetch(`https://airline-ticketing-api.onrender.com/clientProfiles/${_id}/delete`, {
			method: 'DELETE',
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
				console.log(data)
				Swal.fire(
					'Deleted',
					'Passenger Profile is Deleted',
					'success'
				)

				history(0)
				
			})
	}


	return(
		<Fragment>
			<Table className="m-5" striped bordered hover>
				<thead><h4>Passenger</h4></thead>
					<Button variant = 'secondary' className="m-1" onClick={handleShow}>Edit</Button>
					<Button variant = 'secondary' onClick={(e)=> confirmDelete(e)}>Delete</Button>
				<tbody>
					<tr>
						<th width="400px">Name:</th>
							<td>{name}</td>
					</tr>
					<tr>
						<th>Gender:</th>
							<td>{gender}</td>
					</tr>
					<tr>
						<th>Birtday:</th>
							<td>{new Date(birthday).toLocaleDateString()}</td>
					</tr>
					<tr>
						<th>Age:</th>
							<td>{age}</td>
					</tr>
					{(address !== '') ? 
						<tr>
						<th>Address:</th>
							<td>{address}</td>
						</tr>
					: null
					}

					{(email !== '') ? 
						<tr>
						<th>Email:</th>
							<td>{email}</td>
						</tr>
					: null
					}

					{(mobileNo !== '') ? 
						<tr>
						<th>Mobile Number:</th>
							<td>{mobileNo}</td>
						</tr>
					: null
					}

					<tr>
						<th>Passport Number:</th>
							<td>{passportNo}</td>
					</tr>
				</tbody>
			</Table>

			<Modal show={show} onHide={handleClose} size="lg" aria-labelledby="conatained-modal-title-vcenter" centered>
				<Form onSubmit={e => editProfile(e, _id)}>
					<Modal.Header closeButton>
						<Modal.Title id="contained-modal-title-vcenter">EDIT PASSENGER DETAILS: {name}</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Full Name</Form.Label>
								<Form.Control 
				 					type = 'name'				 					
				 					onChange = {e => setNewName(e.target.value)}		 
								/>
						</Form.Group>

						<Row className="mb-3"> 
						<Form.Group  as={Col} >
							<Form.Label>Gender</Form.Label>
								<Form.Control 
									as = "select"
									type = 'gender'				 					
				 					onChange = {e => setNewGender(e.target.value)}		
								>
									<option value="">Select Gender</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</Form.Control>
						</Form.Group>
						

						<Form.Group  as={Col}>
							<Form.Label>Birthday</Form.Label>
								<Form.Control 
				 					type = 'date'				 					
				 					onChange = {e => setNewBirthday(e.target.value)}
				 					
								/>

						</Form.Group>

						<Form.Group  as={Col}>
							<Form.Label>Age</Form.Label>
								<Form.Control 
				 					type = 'number'				 					
				 					onChange = {e => setNewAge(e.target.value)}
				 					
								/>
						</Form.Group>

						</Row>


						<Form.Group>
							<Form.Label>Complete Address*</Form.Label>
								<Form.Control 
				 					type = 'address'				 					
				 					onChange = {e => setNewAddress(e.target.value)}
								/>
						</Form.Group>
						
						<Form.Group>
							<Form.Label>Email*</Form.Label>
								<Form.Control 
				 					type = 'email'				 					
				 					onChange = {e => setNewEmail(e.target.value)}
								/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Mobile No.*</Form.Label>
								<Form.Control 
				 					type = 'mobile'				 					
				 					onChange = {e => setNewMobileNo(e.target.value)}
								/>
						</Form.Group>


						<Form.Group>
							<Form.Label>Passport Number</Form.Label>
								<Form.Control 
				 					type = 'passportNo'				 		
				 					onChange = {e => setNewPassportNo(e.target.value)}
								/>
						</Form.Group>
						<h6><em>* Optional</em></h6>
					</Modal.Body>

					<Modal.Footer>
						<Button  variant = 'secondary' type= 'submit' >Submit</Button>
					</Modal.Footer>	
				</Form>
			</Modal>
		</Fragment>
	)
}