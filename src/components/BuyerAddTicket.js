import {useState, useContext, Fragment} from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
//import {Navigate, useNavigate, Link} from 'react-router-dom'
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function BuyerAddTicket() {

	const {user, setUser} = useContext(UserContext)

	const history = useNavigate()

	//const [buyer, setBuyer] = useState('')
	const [isOneWay, setIsOneWay] = useState(true)
	const [origin, setOrigin] = useState('')
	const [destination, setDestination] = useState('')
	const [airline, setAirline] = useState('')
	const [dateDeparture, setDateDeparture] = useState('')
	const [dateArrival, setDateArrival] = useState('')
	const [adult, setAdult] = useState('1')
	const [child, setChild] = useState('0')
	const [infant, setInfant] = useState('0')
	const [passengers, setPassengers] = useState([])
	const [message, setMessage] = useState('')
 	
 	//const [isActive, setIsActive] = useState(false)

 	function addBuyerTicket(e) {
 		//console.log(e)
 		e.preventDefault();

 		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/createBuyerTicket`, {
 			method: 'POST', 
 			headers: {
 				'Content-Type' : 'application/json',
 				Authorization : `Bearer ${localStorage.getItem('token')}`
 			},
 			body: JSON.stringify({
 					buyer :user._id, 
					isOneWay : isOneWay,
					origin : origin,
					destination : destination,
					airline : airline,
					dateDeparture :dateDeparture,
					dateArrival :dateArrival,
					adult: adult,
					child: child,
					infant :infant,
					passengers:passengers,
					message: message
 			})
 		})
 		.then(res => {
 			return res.json()
 		})
 		.then(data => {
 				console.log(data)

 				if(data){
				Swal.fire({
					title: 'Success',
					icon: 'success'
				})

				history(0)

			} else {
				Swal.fire({
					title: 'Error',
					icon: 'error'
				})
			}
 		})

 		//setBuyer('')
		setIsOneWay(true)
		setOrigin('')
		setDestination('')
		setAirline('')
		setDateDeparture('')
		setDateArrival('')
		setAdult('1')
		setChild('0')
		setInfant('0')
		setPassengers([])
		setMessage('')
 	}
/*
 	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/


	return(
		<Fragment>
			<Row>
				<h1 className="text-center my-5">Airline Ticket Inquiry</h1>
			</Row>
			<Row className="justify-content-center">
					<Col md={6}  >
						<Form onSubmit={(e) => addBuyerTicket(e)}>
							<Form.Group>
								<Form.Control 
									as = "select"
									type = 'isOneWay'
				 					value = {isOneWay}
				 					onChange = {e => setIsOneWay(e.target.value)}
				 					required
								>
								<option value="">Select one</option>
								<option value="true">One Way</option>
									<option value="false">Round Trip</option>
								</Form.Control>
							</Form.Group>

							<Form.Group>
							<Form.Label>Airline</Form.Label>
								<Form.Control 
									as = "select"
									type = 'airline'
				 					value = {airline}
				 					onChange = {e => setAirline(e.target.value)}
				 					required
								>
									<option>Select Airline</option>
									<option value="pal">PAL</option>
									<option value="ceb">CEB</option>
									<option value="air">AIR</option>
									<option value="delta">Delta</option>
								</Form.Control>
							</Form.Group>

 							<Form.Group controlId= "origin">
 								<Form.Label>Origin</Form.Label>
											<Form.Control 
				 								type = 'text'
				 								value = {origin}
				 								onChange = {e => setOrigin(e.target.value)}
				 								required
											/>
										</Form.Group>
							<Form.Group controlId= "destination">
								<Form.Label>Destination</Form.Label>
											<Form.Control 
				 								type = 'text'
				 								value = {destination}
				 								onChange = {e => setDestination(e.target.value)}
				 								required
											/>
										</Form.Group>
							<Row>

 								<Form.Group as={Col} controlId= "dateDeparture">
 									<Form.Label>Departure Date</Form.Label>
											<Form.Control 
				 								type = 'date'
				 								value = {dateDeparture}
				 								onChange = {e => setDateDeparture(e.target.value)}
				 								required
											/>
										</Form.Group>

								<Form.Group as={Col} controlId= "dateArrival">
										 <Form.Label>Arrival Date</Form.Label>
											<Form.Control 
				 								type = 'date'
				 								value = {dateArrival}
				 								onChange = {e => setDateArrival(e.target.value)}
											/>
										</Form.Group>
							</Row>

							<Row className="mb-3">
							    <Form.Group as={Col} controlId="adultpax">
							      <Form.Label>Adult</Form.Label>
							      <Form.Control 
							      	type="number" 
							      	placeholder="1" 
							      	value = {adult}
							      	onChange = {e => setAdult(e.target.value)}
							      />
							    </Form.Group>

							    <Form.Group as={Col} controlId="childpax">
							      <Form.Label>Child</Form.Label>
							      <Form.Control 
							      	type="number" 
							      	placeholder="0" 
							      	value = {child}
							      	onChange = {e => setChild(e.target.value)}
							      	/>
							    </Form.Group>

							     <Form.Group as={Col} controlId="infant">
							      <Form.Label>Infant</Form.Label>
							      <Form.Control 
							      	type="number" 
							      	placeholder="0" 
							      	value = {infant}
							      	onChange = {e => setInfant(e.target.value)}
							      	/>
							    </Form.Group>
							  </Row>

 							<Form.Group controlId= "message">
 							<Form.Label>Message</Form.Label>
									<Form.Control 
											as="textarea"
				 								type = 'text'
				 								value = {message}
				 								onChange = {e => setMessage(e.target.value)}
				 								
											/>
									</Form.Group>

								<Button  variant = 'secondary' type= 'submit' id = 'submitBtn' className= "mt-4">Submit</Button>
						</Form>
					</Col>
			</Row>
		</Fragment>
	)
}

	