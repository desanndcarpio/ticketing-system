import {Fragment} from 'react'
import {Table, Button, Container, Accordion} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function UrgentCard({SUrgentProp}){

	const {_id, userId, status, createdOn, messages, concern, isUrgent} = SUrgentProp

	const history = useNavigate()

	function renderMessages(messages) {
		return messages.map(message => {
			return (
				<tr>
					<td>{message.sender.name} : {message.message} <br/>Date Posted: {new Date(message.postedOn).toLocaleDateString()}</td>
					<hr/>
				</tr>
			)
		})
	}

	function approve(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${_id}/approval`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json', 
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				approval: 'approved'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				title:'Ticket Approved',
				icon:'success'
			})
			history(0)
		})
	}

	function decline(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${_id}/approval`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				approval: 'declined'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title: 'Ticket Declined',
				  icon: 'success'
			})
		history(0)	
		})
	}

	function addMessage(e){
		let {value: newMessage} = Swal.fire({
			title: 'Add Message',
			input: 'text',
			showCancelButton: true,
			preConfirm: (newMessage) => {
				//console.log(newMessage)
				if(newMessage){
					fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${_id}/addMessage`, {
						method: 'POST',
						headers: {
							"Content-Type" : "application/json",
							Authorization : `Bearer ${localStorage.getItem("token")}`
						},
						body: JSON.stringify({
							message: newMessage
						})
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						history(0)
					})
				}
			}
		})
	}

	return(
		<Fragment>
			<Container>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Control No.</th>
							<th>Staff Name</th>
							<th>Concern</th>
							<th>Date Posted</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>{_id}</td>
							<td>{userId.name}</td>
							<td>{concern}</td>
							<td>{new Date(createdOn).toLocaleDateString()}</td>
							<td>{status}</td>
							<td>
							<Button variant="secondary" className="m-2" onClick={(e) => addMessage(e)}>Add message</Button>
							<Button variant="secondary" className="mx-2" onClick={(e) => approve(e)}>Approve</Button> 
							<Button variant="secondary" className="mx-2" onClick={(e) => decline(e)}>Decline</Button>
							</td>
						</tr>
					</tbody>
				</Table>
				<Accordion>
					<Accordion.Item eventKey="0">
						<Accordion.Header>Messages</Accordion.Header>
						<Accordion.Body>
							{renderMessages(messages)}
						</Accordion.Body>
					</Accordion.Item>
				</Accordion>
			</Container>
		</Fragment>
	)	

}