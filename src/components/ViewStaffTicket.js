import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 
import StaffTicketCard from '../components/StaffTicketCard'

export default function ViewStaffTicket(){

	const {user, setUser} = useContext(UserContext)

	const [staffTicket, setStaffTicket] = useState([])

/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/
/*
	useEffect(() => {
		fetch(`https://secure-plateau-49977.herokuapp.com/staffTickets/myTicket`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setStaffTicket(data.map(staffTicket=> {
				return(
					<Fragment>
						<StaffTicketCard key= {staffTicket.id} ViewStaffTicketProp= {staffTicket}/>
					</Fragment>
				)
			}))
		})
	}, [])


	return(
		<Fragment>
			<Container>
				<Row>
					<h1 className="my-5">Incident Tickets</h1>
					<Col>
						{staffTicket}
					</Col>
				</Row>
			</Container>	
		</Fragment>
	)*/

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/myTicket`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setStaffTicket(data)
		})
	}, [])

	//console.log(staffTicket)
	function renderStaffTickets(staffTicket){
		return staffTicket.map(i => {
			return(
				<tr>
					<td><Link to = {`/staffTickets/${i._id}`}>{i._id}</Link></td>
					<td>{i.concern}</td>
					<td>{i.subject}</td>
					<td>{i.userId.name}</td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.status}</td>
					<td>{i.approval}</td>
				</tr>
			)
		})
	}

	return(
		<Fragment>
			<Container>
				<Row className="m-5">
					<Col className="mt-5">
						<Table>
							<thead>
								<tr>
									<th>Control Number</th>
									<th>Concern</th>
									<th>Subject</th>
									<th>Posted By</th>
									<th>Date Created</th>
									<th>Status</th>
									<th>Approval</th>
								</tr>
							</thead>

							<tbody>
								{(staffTicket.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderStaffTickets(staffTicket)}
							</tbody>
						</Table>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}