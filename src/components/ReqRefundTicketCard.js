import {useState, useEffect, Fragment} from 'react'
import {Table, Button, Row, Container, Col, Accordion} from 'react-bootstrap'
//import {FaTrashAlt} from "react-icons/fa";
import {useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
//import UserContext from '../UserContext' 

export default function ReqRefundTicketCard({ReqRefundTicketProp}){

	//const {_id, buyer, isOneWay, origin, destination, airline, dateDeparture, dateArrival, adult, child, infant, passengers, messages, createdOn, isActive, status, isPaid, availability, totalAmount, approval} = ViewBuyerTicketProp
	
	const {_id, isOneWay, origin, destination, airline, dateDeparture, dateArrival, adult, child, infant, passengers, messages, createdOn, isPaid, availability, totalAmount, isReqRefund} = ReqRefundTicketProp
	
	const [profiles, setProfiles] = useState([])
	// const {user} = useContext(UserContext)

	const history = useNavigate()

	//console.log(messages)
	//console.log(passengers)

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/clientProfiles/profile`, {
			headers : {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setProfiles(data)
		})
	}, [])

	//console.log(profiles)

	
	function addPassenger(e){
		
		const inputOptions = profiles
		
		let {value : newPassenger} = Swal.fire({
			title : 'Add Passenger',
			input: 'select',
			inputOptions : {
				'passenger': profiles.map(profile => profile.name)
			},
			inputPlaceholder: 'Add Passenger',
			showCancelButton : true,
			inputValidator: (index) => {
				console.log(inputOptions[index]._id);
				if(index){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/addPassenger`, {
						method: 'POST',
						headers: {
							"Content-Type" : "application/json",
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							passengerId : inputOptions[index]._id
						})
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						alert('ok')
					})
				}
			}
		})
		
	}

	function renderMessages (messages) {
		return messages.map(message => {
			//console.log(message)
			return (

				<tr>
					<td>{message.sender.name} : {message.message} <br/>Date Posted: {new Date(message.postedOn).toLocaleDateString()}</td>
					<hr/>
				</tr>
			)
		
		})
	}



	function renderPassengers (passengers) {
		return passengers.map(passenger => {
			//console.log(passenger.passengerId.name)
			return (
				<li>{passenger.passengerId.name}</li>
			)
		})
	}



	function addMessage(e){
		let {value: newMessage} = Swal.fire({
			title: 'Add Message',
			input: 'text',
			showCancelButton: true,
			preConfirm: (newMessage) => {
				//console.log(newMessage)
				if(newMessage){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/addMessage`, {
						method: 'POST',
						headers: {
							"Content-Type" : "application/json",
							Authorization : `Bearer ${localStorage.getItem("token")}`
						},
						body: JSON.stringify({
							message: newMessage
						})
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						history(0)
					})
				}
			}
		})
	}



	function reqRefund(e){
		let reqRefundMessage = 'BUYER REQUEST FOR REFUND.'	

		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/addMessage`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				message: reqRefundMessage,
				isReqRefund: true
			})
		})
		.then(res=> {
			return res.json()
		})
		.then(data => {
			history(0)
	})
	}

	function reqCancel(e){
		let reqCancelMessage = 'BUYER REQUEST TO CANCEL.'	

		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/addMessage`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				message: reqCancelMessage
			})
		})
		.then(res=> {
			return res.json()
		})
		.then(data => {
			history(0)
	})
	}


	function confirmDelete(e){
		Swal.fire({
  			title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Yes, delete it!'
		})
		.then((result) => {
			//console.log(result)
			if(result.isConfirmed){
				deleteTicket(result)
			}
		})
	}

	function deleteTicket(e){
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/delete`, {
			method: 'DELETE',
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire(
				'Deleted',
				'Your Ticket is Deleted',
				'success'	
			)

			history(0)
		})
	}


	
	//console.log(passengers)
	// console.log(typeof({adult}))
	// console.log({child})

	//console.log(new Date(createdOn).toLocaleDateString())
	//console.log(availability)
	return(
		<Fragment>
			<Container>
				<h1 className="mt-5">Ticket Refund Request</h1>
				<Row className="mt-5">
					<Col>
					{/*{renderProfiles(profiles)}*/}
					<hr/>
						<h5>Control Number: {_id}</h5>
						<p>Date Posted: {new Date(createdOn).toLocaleDateString()}</p>
						{(isPaid || availability === "available") ? null : <Button  variant = 'secondary' className="mb-2" onClick={(e)=> confirmDelete(e)}>Delete Ticket</Button> }
		
						<Table striped bordered hover>
							<thead>
								<tr>
									
									<th>  </th>
									<th>Airline</th>
									<th>Origin</th>
									<th>Destination</th>
									<th>Departure Date</th>
									{isOneWay ? null : <th>Arrival Date</th>}			
									{(adult !== 0) ? <th>Adult</th> : null}
									{(child !== 0) ? <th>Child</th> : null}
									{(infant !== 0) ? <th>Infant</th> : null}
									<th>Passengers</th>
									{(totalAmount !== '0') ? <th>Total Amount</th> : null}
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{isOneWay ? 'One Way' : 'Round Trip'}</td>
									<td>{airline}</td>
									<td>{origin}</td>
									<td>{destination}</td>
									<td>{new Date(dateDeparture).toLocaleDateString()}</td>
									{isOneWay ? null : <td>{new Date(dateArrival).toLocaleDateString()}</td>}
									{(adult !== 0) ? <td>{adult}</td> : null}
									{(child !== 0) ? <td >{child}</td> : null}
									{(infant !== 0) ? <td>{infant}</td>: null}
									
									{(passengers.length === 0) ? 
										<td>
											No saved passenger profiles yet.
											<Button variant="link" as={Link} to="/clientProfile"> Click here to add</Button>
										</td>
									:
										<td>
											<ul>
												{renderPassengers(passengers)}
											</ul>
										</td>	
									}								
									{(totalAmount !== '0') ? <td>{totalAmount}</td> : null}
									<td>
										<Button variant = 'secondary' className="m-2" onClick={(e) => addMessage(e)}>Add message</Button>
									</td>
								</tr>
							</tbody>
						</Table>
						<Accordion>
								<Accordion.Item eventKey="0">
									<Accordion.Header>Messages</Accordion.Header>
										<Accordion.Body>
											{renderMessages(messages)}
										</Accordion.Body>
									</Accordion.Item>
						</Accordion>
						<hr/>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}