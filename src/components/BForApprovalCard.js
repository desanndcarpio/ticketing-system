//import {useState, useEffect, useContext, Fragment} from 'react'
import {Fragment} from 'react'
import {Table, Button, Accordion} from 'react-bootstrap'
//import {FaTrashAlt} from "react-icons/fa";
//import {Navigate, useNavigate, Link} from 'react-router-dom'\
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
//import UserContext from '../UserContext' 

export default function BForApprovalCard({BForApprovalProp}){

	//const {_id, buyer, isOneWay, origin, destination, airline, dateDeparture, dateArrival, adult, child, infant, passengers, messages, createdOn, isActive, status, approval, isPaid, availability, totalAmount} = BForApprovalProp

	const {_id, buyer, isOneWay, origin, destination, airline, dateDeparture, dateArrival, messages, status, totalAmount} = BForApprovalProp
	//console.log(BPendingProp)
	
	const history = useNavigate()

	function renderMessages (messages) {
		return messages.map(message => {
			//console.log(message)
			return (
				<tr>
					<td>{message.sender.name} : {message.message}</td>
				</tr>
			)
		})
	}

/*	function addMessage(e){
		let {value: newMessage} = Swal.fire({
			title: 'Add Message',
			input: 'text',
			showCancelButton: true,
			preConfirm: (newMessage) => {
				//console.log(newMessage)
				if(newMessage){
					fetch(`http://localhost:4000/buyerTickets/${_id}/addMessage`, {
						method: 'POST',
						headers: {
							"Content-Type" : "application/json",
							Authorization : `Bearer ${localStorage.getItem("token")}`
						},
						body: JSON.stringify({
							message: newMessage
						})
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						history(0)
					})
				}
			}
		})
	}

*/

	function approve(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${_id}/approval`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				approval: 'approved'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title:'Ticket Approved',
				  icon:'success'
			})
			history(0)
		})
	}

	function decline(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${_id}/approval`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				approval: 'declined'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title: 'Ticket Declined',
				  icon: 'success'
			})
		history(0)	
		})
	}


	return(
		<Fragment>
			<h5>Control Number: {_id}</h5>
			<h5>Customer: {buyer.name}</h5>
			<h6>Email: {buyer.email}</h6>
		<Table striped bordered hover>
			<thead>
				<tr>
					
					<th>  </th>
					<th>Airline</th>
					<th>Origin</th>
					<th>Destination</th>
					<th>Departure Date</th>
					{isOneWay ? null : <th>Arrival Date</th>}
					<th>Status</th>
					{(totalAmount !== '0') ? <th>Total Amount</th> : null}
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{isOneWay ? 'One Way' : 'Round Trip'}</td>
					<td>{airline}</td>
					<td>{origin}</td>
					<td>{destination}</td>
					<td>{new Date(dateDeparture).toLocaleDateString()}</td>
					{isOneWay ? null : <td>{new Date(dateArrival).toLocaleDateString()}</td>}
					<td>{status}</td>
					{(totalAmount !== '0') ? <td>{totalAmount}</td> : null}
					<td>
						<Button variant="secondary" className="mx-2" onClick={(e) => approve(e)}>Approve</Button> 
						<Button variant="secondary" className="mx-2" onClick={(e) => decline(e)}>Decline</Button>
					</td>
				</tr>
			</tbody>
		</Table>
		<Accordion className="mb-4">
			<Accordion.Item eventKey="0">
				<Accordion.Header>Messages</Accordion.Header>
				<Accordion.Body>
					{renderMessages(messages)}
				</Accordion.Body>
			</Accordion.Item>
		</Accordion>
		</Fragment>
	)
}