import {useState, useEffect, useContext, Fragment} from 'react';
import {Row, Col, Form, Button, Card, Container} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';
import '../App.css'

export default function Login(){

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(true)

	const history = useNavigate()

	function authenticate(e){

		e.preventDefault();

		fetch(`https://airline-ticketing-api.onrender.com/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful!',
					icon: 'success',
					text: 'Welcome'
				})

				console.log(data.access)
				history('/')
			} else {

				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Check your login details'
				})
			}
		})

		setEmail('');
		setPassword('');
	}

	const retrieveUserDetails = (token) =>{
		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}

	useEffect (() => {
		if(email !== '' && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])


	/*

<Row className="m-0 " style={{height: "660px", width: '30rem'}} id="log-in-card">
				<Card className="text-black my-auto" border="0" style={{ height: '28rem' }} >
					 	<Row className="m-0 mt-5 p-0">
							<Col className=" text-center m-5">
	*/

	return(

	/*	(user.id !== null) ?

			<Navigate to="/" />
		:*/

		<Fragment >
				<Col  >
					<Card className="mt-5 p-5 text-center" id="log-in-card">
								<Form onSubmit={(e)=>authenticate(e)}>
									<Form.Group controlId= "userEmail">
										<Form.Label></Form.Label>
										<Form.Control 
											type= 'email'
											placeholder= 'Enter email'
											value = {email} 
											onChange = {(e) => setEmail(e.target.value)}
											required
										/>
										</Form.Group>

									<Form.Group controlId = "password" className="my-4">
										<Form.Label></Form.Label>
										<Form.Control 
											type = "password"
											placeholder = "Password"
											value = {password}
											onChange = {(e) => setPassword(e.target.value)}
											required
										/>
									</Form.Group>

									{ isActive ? 
										<Button variant = 'secondary' type= 'submit' id = 'submitBtn' >
											Submit
										</Button>

										: 
										<Button variant = 'secondary' type= 'submit' id = 'submitBtn' disabled>
											Submit
										</Button>
									}
										<Button variant = 'secondary'as={Link} to="/register" className="mx-4" >
											Signup Here
										</Button>

								</Form>
					</Card>
				</Col>
		
		</Fragment>
	)

}