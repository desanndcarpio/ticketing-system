import {Fragment , useContext} from 'react'
import {Carousel} from 'react-bootstrap'
import {Link} from 'react-router-dom' 
import UserContext from '../UserContext'
import '../App.css';

export default function HomeBanner(){

	const {user} = useContext(UserContext)

	return(
		<Carousel fade id="carousel">
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="../images/banner.jpg"
		      alt="First slide"
		      height={700}
		    />
		    <Carousel.Caption>
		      <h3>First slide label</h3>
		      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="../images/banner1.png"
		      alt="Second slide"
		      height={700}
		    />

		    <Carousel.Caption>
		      <h3>Second slide label</h3>
		      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src="../images/banner2.jpg"
		      alt="Third slide"
		      height={700}
		    />

		    <Carousel.Caption>
		      <h3>Third slide label</h3>
		      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>
	)
}