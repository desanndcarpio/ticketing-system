import {Fragment , useContext} from 'react'
import {Nav, Navbar, NavDropdown} from 'react-bootstrap'
//import {FaCartArrowDown} from "react-icons/fa";
import {Link} from 'react-router-dom' 
import UserContext from '../UserContext'
import '../App.css';

export default function AppNavbar(){

	const {user} = useContext(UserContext)



	return(
		<Navbar id="navbar" expand="lg" className="m-0 p-4">
			<Navbar.Brand as={Link} to="/" exact >Ahkbar Alam Tours</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="justify-content-end" style={{ width: "100%" }}>
					<Nav.Link as={Link} to="/" exact >Home</Nav.Link>
					
					

					{(user.id !== null && user.role === 'user') ?
						<Fragment>
						   <NavDropdown title="Ticket" id="basic-nav-dropdown">
					        <NavDropdown.Item as={Link} to="/buyerTicket" exact >Issuance Ticket</NavDropdown.Item>
					        <NavDropdown.Item as={Link} to="/ReqRefundTicket" exact >Refund Request</NavDropdown.Item>
					      </NavDropdown>
						
						<Nav.Link as={Link} to="/clientProfile" exact >Passengers</Nav.Link>
						</Fragment>

						: (user.id !== null && user.role === 'staff') ?
						<Fragment>
							<Nav.Link as={Link} to="/staffTicket" exact >StaffTicket</Nav.Link>
							<Nav.Link as={Link} to="/sbDashboard" exact >Dashboard</Nav.Link>
						</Fragment> 

						: (user.id !== null && user.role === 'manager') ?
						<Fragment>
							<Nav.Link as={Link} to="/MDashboard" exact >Dashboard</Nav.Link>
						</Fragment>

						: (user.id !== null && user.role === 'admin') ?
						<Fragment>
							<Nav.Link as={Link} to="/ADashboard" exact >Dashboard</Nav.Link>
						</Fragment> 

						: (user.id !== null && user.role === 'it') ?
						<Fragment>
							<Nav.Link as={Link} to="/IDashboard" exact >Dashboard</Nav.Link>
						</Fragment> 

						: null
					}

					{user.id === null ? <Nav.Link as={Link} to="/login" exact >Login</Nav.Link> : null}
					{(user.id !== null) ?
						<Fragment>
							<Nav.Link as={Link} to="/MyProfile" exact >My Profile</Nav.Link> 
							<Nav.Link as={Link} to="/logout" exact >Logout</Nav.Link> 
						</Fragment>
					 : null }
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)


}