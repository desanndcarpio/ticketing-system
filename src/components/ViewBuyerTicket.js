import {Fragment, useContext, useState, useEffect} from 'react'
//import {Link} from 'react-router-dom'
import {Col, Row, Container} from 'react-bootstrap'
import UserContext from '../UserContext' 
import BuyerTicketCard from '../components/BuyerTicketCard'


export default function ViewBuyerTicket(){

	const {user, setUser} = useContext(UserContext)

	const [buyerTicket, setBuyerTicket] = useState([])
	


/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}
*/
	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/myTicket`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setBuyerTicket(data.map(buyerTicket=> {
				return(
					<Fragment>
						<BuyerTicketCard key= {buyerTicket.id} ViewBuyerTicketProp= {buyerTicket}/>
					</Fragment>
				)
			}))
		})
	}, [])

	

	return(

		<Fragment>
			<Container>
				<Row>
					<Col>
						{buyerTicket}
					</Col>
				</Row>
			</Container>	
		</Fragment>

	)
}