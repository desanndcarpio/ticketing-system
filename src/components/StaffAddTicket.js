import {useState, useContext, Fragment} from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import {Navigate, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function StaffAddTicket() {

	const {user, setUser} = useContext(UserContext)

	const history = useNavigate()

	//const [userId, setUserId] = useState('')
	const [concern, setConcern] = useState('')
	const [subject, setSubject] = useState('')
	const [message, setMessage] = useState('')
	const [isUrgent, setIsUrgent] = useState(false)
	//const [imgURL, setImgURL] = useState('')

	const retrieveUserDetails = (token) =>{
		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}

	function addStaffTicket(e) {
		e.preventDefault();

		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/createStaffTicket`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user._id,
				concern: concern,
				subject: subject,
				message: message,
				isUrgent: isUrgent
				//imgURL: imgURL
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
				  icon: 'success'
			})
		})

		setConcern('')
		setMessage('')
		setSubject('')
		setIsUrgent(false)
		//setImgURL('')
	}

	return(

		<Fragment>
			<Row>
				<h1 className="my-5 text-center">Add Incident Ticket</h1>
			</Row>

			<Row>
				<Col md={6} className="mx-auto">
					<Form onSubmit={(e) => addStaffTicket(e)}>

						<Form.Group className="mb-3">
							<Form.Control
								as="select"
								type = 'concern'
								value = {concern}
								onChange = {e => setConcern(e.target.value)}
								required
							>
								<option >Select Concern</option>
								<option value="addCredits">Request Additional Credits</option>
								<option value="maintenance">Maintenance Issue</option>
								<option value="dBError">Database Error</option>
								<option value="others">Others - specify concern in message box</option>
							</Form.Control>
						</Form.Group>

						<Form.Group className="mb-3">
							<Form.Label>Is your concern need urgent attention?</Form.Label>
							<Form.Control
								as="select"
								type = 'isUrgent'
								value = {isUrgent}
								onChange = {e => setIsUrgent(e.target.value)}
								required
							>
								<option value="false">No</option>
								<option value="true">Yes</option>
							</Form.Control>
						</Form.Group>						

						<Form.Group>
							<Form.Label>Subject</Form.Label>
							<Form.Control
								type = 'text'
								value = {subject}
								onChange = {e => setSubject(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Message</Form.Label>
							<Form.Control
								as="textarea"
								type = 'text'
								value = {message}
								onChange = {e => setMessage(e.target.value)}
							/>
						</Form.Group>

						<Button  variant="secondary"  type='submit' className="mt-3">Submit</Button>
					</Form>
				</Col>
			</Row>
		</Fragment>
	)
}