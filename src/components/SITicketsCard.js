import {useContext, Fragment} from 'react'
import {Table, Button, Container, Accordion} from 'react-bootstrap'
//import {FaTrashAlt} from "react-icons/fa";
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext' 

export default function SITicketsCard({SITicketsProp}){

	const {_id, userId, createdOn, status, messages, concern} = SITicketsProp

	const history = useNavigate()

	const {user, setUser} = useContext(UserContext)

	function renderMessages(messages) {
		return messages.map(message => {
			return (
				<tr>
					<td>{message.sender.name} : {message.message} <br/>Date Posted: {new Date(message.postedOn).toLocaleDateString()}</td>
					<hr/>
				</tr>
			)
		})
	}

	function resolved(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${_id}/status`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: 'resolved'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title:'Ticket Resolved',
				  icon:'success'
			})
			history(0)
		})
	}

	function unresolved(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${_id}/status`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: 'unresolved'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title: 'Ticket Unresolved',
				  icon: 'success'
			})
		history(0)	
		})
	}



	return(
		<Fragment>
			<Container>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Control No.</th>
							<th>Staff Name</th>
							<th>Concern</th>
							<th>Date Posted</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>{_id}</td>
							<td>{userId.name}</td>
							<td>{concern}</td>
							<td>{new Date(createdOn).toLocaleDateString()}</td>
							<td>{status}</td>
							<td>
							<Button variant="secondary" className="mx-2" onClick={(e) => resolved(e)}>Resolved</Button> 
							<Button variant="secondary" className="mx-2" onClick={(e) => unresolved(e)}>Unresolved</Button>
							</td>
						</tr>
					</tbody>	
				</Table>
				<Accordion>
						<Accordion.Item eventKey="0">
							<Accordion.Header>Messages</Accordion.Header>
							<Accordion.Body>
								{renderMessages(messages)}
							</Accordion.Body>
						</Accordion.Item>
				</Accordion>
			</Container>
		</Fragment>
	)

}