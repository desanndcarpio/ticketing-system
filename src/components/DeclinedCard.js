import {useState, useEffect, useContext, Fragment} from 'react'
import {Image, Table, Button, Row, Container, CardGroup, Card, Accordion} from 'react-bootstrap'
//import {FaTrashAlt} from "react-icons/fa";
import {Navigate, useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext' 

export default function DeclinedCard({SDeclinedProp}){

	const {_id, userId, createdOn, isActive, status, messages, concern, subject, approval} = SDeclinedProp

	const history = useNavigate()

	//const {user, setUser} = useContext(UserContext)

	function renderMessages (messages) {
		return messages.map(message => {
			//console.log(message)
			return (
				<tr>
					<td>{message.sender.name} : {message.message} <br/>Date Posted: {new Date(message.postedOn).toLocaleDateString()}</td>
					<hr/>
				</tr>
			)
		})
	}

	return(
		<Fragment>

			<tr>
				<td>{_id}</td>
				<td>{userId.name}</td>
				<td>{new Date(createdOn).toLocaleString()}</td>
				<td>{concern}</td>
				<th>{subject}</th>
				<td>
					<Accordion>
						<Accordion.Item eventKey="0">
							<Accordion.Header> </Accordion.Header>
							<Accordion.Body>
								{renderMessages(messages)}
							</Accordion.Body>
						</Accordion.Item>
					</Accordion>
				</td>
			</tr>
		
		</Fragment>
	)

}