import {Fragment} from 'react'
import {Table, Button, Accordion} from 'react-bootstrap'
//import {FaTrashAlt} from "react-icons/fa";
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
//import UserContext from '../UserContext' 

export default function StaffTicketCard({ViewStaffTicketProp}){

	const {_id, userId, concern, messages, status, approval, createdOn} = ViewStaffTicketProp

	const history = useNavigate()

	function renderMessages (messages) {
		return messages.map(message => {
			return(

				<tr>
					<td>{message.sender.name} : {message.message} <br/> {message.postedOn}</td>
				</tr>	
			)
		})
	}

	function addMessage(e){
		let {value: newMessage} = Swal.fire({
			title: 'Add Message',
			input: 'text',
			showCancelButton: true,
			preConfirm: (newMessage) => {
				//console.log(newMessage)
				if(newMessage){
					fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${_id}/addMessage`, {
						method: 'POST',
						headers: {
							"Content-Type" : "application/json",
							Authorization : `Bearer ${localStorage.getItem("token")}`
						},
						body: JSON.stringify({
							message: newMessage
						})
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						history(0)
					})
				}
			}
		})
	}

	return(
		<Fragment>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Control No.</th>
						<th>Concern</th>
						<th>Date Posted</th>
						<th>Status</th>
						<th>Approval</th>
						<th>Actions</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>{_id}</td>
						<td>{concern}</td>
						<td>{new Date(createdOn).toLocaleDateString()}</td>
						<td>{status}</td>
						<td>{approval}</td>
						<td>
							<Button variant="secondary" onClick={(e) => addMessage(e)}>Add Message</Button>
						</td>
					</tr>
				</tbody>
			</Table>
			<Accordion className="mb-5">
				<Accordion.Item eventKey="0">
					<Accordion.Header>Messages</Accordion.Header>
					<Accordion.Body>
						{renderMessages(messages)}
					</Accordion.Body>
				</Accordion.Item>
			</Accordion>			
		</Fragment>
	)
}