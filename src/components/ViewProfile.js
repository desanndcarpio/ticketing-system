import {Fragment, useContext, useState, useEffect} from 'react'
//import {Link} from 'react-router-dom'
import {Col, Row, Container} from 'react-bootstrap'
import UserContext from '../UserContext' 

import ProfileCard from '../components/ProfileCard'

export default function ViewProfile(){

	const {user, setUser} = useContext(UserContext)

	const [profile, setProfile] = useState([])

/*
	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/clientProfiles/profile`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setProfile(data.map(profile => {
				return(
					<Fragment>
						<ProfileCard key= {profile.id} ViewProfileProp = {profile}/>
					</Fragment>
				)
			}))
		})
	}, [])

	return(
		<Fragment>
			<Container>
				<Row>
					<Col>
						{profile}
					</Col>
				</Row>
			</Container>	
		</Fragment>
	)
}