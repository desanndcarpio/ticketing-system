import {useState, useContext, Fragment} from 'react'
import {Table, Button, Row, Col, Container, Modal, Form, Accordion, Card, CardGroup} from 'react-bootstrap'
//import {FaTrashAlt} from "react-icons/fa";
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext' 

export default function PendingCard({BPendingProp}){

	const {_id, buyer, isOneWay, origin, destination, airline, dateDeparture, dateArrival, adult, child, infant, passengers, messages, createdOn, status, approval, isPaid, availability, totalAmount} = BPendingProp

	const {ticketData, fetchData} = BPendingProp

	const [show, setShow] = useState(false);

	const history = useNavigate()

	const [newIsOneWay, setNewIsOneWay] = useState('');
	const [newAirline, setNewAirline] = useState('');
	const [newOrigin, setNewOrigin] = useState('');
	const [newDestination, setNewDestination] = useState('');
	const [newDateDeparture, setNewDateDeparture] = useState('');
	const [newDateArrival, setNewDateArrival] = useState('');
	const [newAdult, setNewAdult] = useState('');
	const [newChild, setNewChild] = useState('0');
	const [newInfant, setNewInfant] = useState('0');
	//const [newPassengers, setNewPassengers] = useState('');


	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const {user, setUser} = useContext(UserContext)

	const editTicket = (e, ticketId) => {
		e.preventDefault();

		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/details`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}, 
			body: JSON.stringify({
				isOneWay : newIsOneWay,
				airline : newAirline,
				origin : newOrigin,
				destination : newDestination,
				dateDeparture : newDateDeparture,
				dateArrival : newDateArrival,
				adult : newAdult,
				child: newChild,
				infant: newInfant,
				// passengers: [
				// 	{
				// 		passengerId: reqBody.passengerId
				// 	}
				// ]
			})
		})
		.then(res=> {
			//console.log(res)
			return res.json()
		})
		.then(data => {

			if(data) {

			

				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Ticket Details Updated'
				})

				setNewIsOneWay('')
				setNewAirline('')
				setNewDestination('')
				setNewDateDeparture('')
				setNewDateArrival('')
				setNewAdult('')
				setNewChild('')
				setNewInfant('')
				//history(0)

				closeEdit();
			} else {

				fetchData();

				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Error'
				})
			}
		})

		//setShow(true)

	}

	const closeEdit = () => {
		setShow(false)
		setNewIsOneWay('')
		setNewAirline('')
		setNewDestination('')
		setNewDateDeparture('')
		setNewDateArrival('')
		setNewAdult('')
		setNewChild('')
		setNewInfant('')
	}


	function renderMessages (messages) {
		return messages.map(message => {
			//console.log(message)
			return (
				<tr>
					<td>{message.sender.name} : {message.message} <br/>Date Posted: {new Date(message.postedOn).toLocaleDateString()}</td>
					<hr/>
				</tr>
			)
		})
	}

	function renderPassengers(passengers){
		return passengers.map(passenger => {
			return(
				<CardGroup className="mb-4">
					<Card>
						<Card.Body>
							<Card.Text className="mb-2">Passenger Name:</Card.Text>
							<Card.Text className="mb-2">Passport Number:</Card.Text>
							<Card.Text className="mb-2">Birthday:</Card.Text>
							<Card.Text className="mb-2">Gender:</Card.Text>
						</Card.Body>
					</Card>

					<Card>
						<Card.Body>
							<Card.Text className="mb-2">{passenger.passengerId.name}</Card.Text>
							<Card.Text className="mb-2">{passenger.passengerId.passportNo}</Card.Text>
							<Card.Text className="mb-2">{new Date(passenger.passengerId.birthday).toLocaleDateString()}</Card.Text>
							<Card.Text className="mb-2">{passenger.passengerId.gender}</Card.Text>
						</Card.Body>
					</Card>
				</CardGroup>
			)
		})
	}

	function addMessage(e){
		let {value: newMessage} = Swal.fire({
			title: 'Add Message',
			input: 'text',
			showCancelButton: true,
			preConfirm: (newMessage) => {
				//console.log(newMessage)
				if(newMessage){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/addMessage`, {
						method: 'POST',
						headers: {
							"Content-Type" : "application/json",
							Authorization : `Bearer ${localStorage.getItem("token")}`
						},
						body: JSON.stringify({
							message: newMessage
						})
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						history(0)
					})
				}
			}
		})
	}



	function changeStatus(e){
		const {value: newStatus} = Swal.fire({
			title: 'Select Status',
			input: 'select',
			inputOptions: {
				'inProgress' : 'In Progress',
				'revision' : 'Pending',
				'refund' : 'Refund',
				'success' : 'Success',
				'cancelled' : 'Cancelled'
			},
			inputPlaceholder: 'Select new Status',
			showCancelButton: true,
			inputValidator : (value) => {
				if(value){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/status`, {
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							status: value
						})
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
						Swal.fire({
				  			title: 'Ticket Status Changed',
				 			icon: 'success'
						})
						history(0)
					})
				}
			}
		})
	}

	function forApproval(e){


		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/approval`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				approval: 'forApproval'
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			Swal.fire({
				 title: 'Ticket For Approval',
				 icon:  'success'
			})
			history(0)
		})
	}

	function changeAvailability(e){
		let {value: newAvailability} = Swal.fire({
			title: 'Ticket Availablity',
			input: 'select',
			inputOptions : {
				'available': 'Available',
				'unavailable' : 'Unavailable',
				'waitListed' : 'Wait Listed'

			},
			inputPlaceholder: 'Select Availability',
			showCancelButton: true,
			inputValidator: (value) => {
				if(value){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/availability`,{
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							Authorization : `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							availability : value
						})
					})
					.then(res=> res.json())
					.then(data => {
						Swal.fire({
				  			title: 'Ticket Availablity Changed',
				 			icon: 'success'
						})
						history(0)
					})
				}
			}
		})
	}


	function addTotalAmount(e){
		let {value : totalAmount} = Swal.fire({
			title: 'Total Amount',
			input: 'text',
			showCancelButton: true,
			preConfirm : (value) => { 
				if(value){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${_id}/addTotalAmount`, {
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							totalAmount: value
						})
					})
					.then(res=> res.json())
					.then(data => {
						Swal.fire(
				  			'Total Amount Added',
				 			'success'
						)
						history(0)
					})
				}
			}
		})
	}

	return(
		<Fragment>
			<Container>
				<Row className="m-5">
					<Col className="mt-5">
						<h5>Control Number: {_id}</h5>
						<h5>Customer: {buyer.name}</h5>
						<h6>Email: {buyer.email}</h6>
					<Table striped bordered hover>
						<thead>
							<tr>
								
								<th>  </th>
								<th>Airline</th>
								<th>Origin</th>
								<th>Destination</th>
								<th>Departure Date</th>
								{isOneWay ? null : <th>Arrival Date</th>}			
								{(adult !== 0) ? <th>Adult</th> : null}
								{(child !== 0) ? <th>Child</th> : null}
								{(infant !== 0) ? <th>Infant</th> : null}
								<th>Passengers</th>
								{(totalAmount !== '0') ? <th>Total Amount</th> : null}
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{isOneWay ? 'One Way' : 'Round Trip'}</td>
								<td>{airline}</td>
								<td>{origin}</td>
								<td>{destination}</td>
								<td>{new Date(dateDeparture).toLocaleDateString()}</td>
								{isOneWay ? null : <td>{new Date(dateArrival).toLocaleDateString()}</td>}
								{(adult !== 0) ? <td>{adult}</td> : null}
								{(child !== 0) ? <td >{child}</td> : null}
								{(infant !== 0) ? <td>{infant}</td>: null}
								<td>Passenger List</td>
								{(totalAmount !== '0') ? <td>{totalAmount}</td> : null}
								
							</tr>
						</tbody>
					</Table>

					<Table>
						<thead>
							<tr>
								<th>Status</th>
								<th>Availablity</th>
								{(approval !== undefined) ? <th>Approval</th> : null }
								<th>Payment Status</th>
								<th>Actions</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td>{status}</td>
								<td>{availability}</td>
								{(approval !== undefined) ? <td>{approval}</td> : null}
								<td>{isPaid ? 'Paid' : 'Pending'}</td>
								<td>
									<Button variant="secondary" className="m-2" onClick={(e) => addMessage(e)}>Add message</Button>
									<Button variant="secondary" className="m-2" onClick={(e) => changeStatus(e)}>Change Status</Button>
									<Button variant="secondary" className="m-2" onClick={handleShow}>Change Details</Button>
									<Button variant="secondary" className="m-2" onClick={(e) => forApproval(e)}>For Approval</Button>
									<Button variant="secondary" className="m-2" onClick={(e) => changeAvailability(e)}>Available</Button>
									<Button variant="secondary" className="m-2" onClick={(e) => addTotalAmount(e)}>{(totalAmount != 0) ? 'Change Total Amount' : 'Add Total Amount'}</Button>
								</td>
							</tr>
						</tbody>
					</Table>
					<Accordion className="mb-3">
						<Accordion.Item eventKey="0">
							<Accordion.Header>Messages</Accordion.Header>
							<Accordion.Body>
								{renderMessages(messages)}
							</Accordion.Body>
						</Accordion.Item>
					</Accordion>
					<hr/>

					<Accordion>
						<Accordion.Item eventKey="0">
							<Accordion.Header>Passenger Details</Accordion.Header>
							<Accordion.Body>
								{(passengers.length !== 0) ? renderPassengers(passengers) : 'No Passenger Details Available'}
							</Accordion.Body>
						</Accordion.Item>
					</Accordion>
					<hr/>


						<Modal show={show} onHide={handleClose} size="lg" aria-labelledby="conatained-modal-title-vcenter" centered>
						<Form onSubmit={e => editTicket(e, _id)}>
							<Modal.Header closeButton>
								<Modal.Title  id="contained-modal-title-vcenter">EDIT TICKET DETAILS: {_id}</Modal.Title>
							</Modal.Header>

							<Modal.Body>
								<Form.Group>
										<Form.Control 
												as = "select"
												type = 'isOneWay'
							 					//value = {isOneWay}
							 					onChange = {e => setNewIsOneWay(e.target.value)}
							 					required
											>
												<option value="">Select one</option>
												<option value="true">One Way</option>
												<option value="false">Round Trip</option>
											</Form.Control>
										</Form.Group>

										<Form.Group>
										<Form.Label>Airline</Form.Label>
											<Form.Control 
												as = "select"
												type = 'airline'
							 					//value = {airline}
							 					onChange = {e => setNewAirline(e.target.value)}
							 					required
											>
												<option value="">Select one</option>
												<option value="pal">PAL</option>
												<option value="ceb">CEB</option>
												<option value="air">AIR</option>
												<option value="delta">Delta</option>
											</Form.Control>
										</Form.Group>

			 							<Form.Group controlId= "origin">
			 								<Form.Label>Origin</Form.Label>
														<Form.Control 
							 								type = 'text'
							 								//value = {origin}
							 								onChange = {e => setNewOrigin(e.target.value)}
							 								required
														/>
										</Form.Group>

										<Form.Group controlId= "destination">
											<Form.Label>Destination</Form.Label>
														<Form.Control 
							 								type = 'text'
							 								//value = {destination}
							 								onChange = {e => setNewDestination(e.target.value)}
							 								required
														/>
										</Form.Group>

										<Row>
			 								<Form.Group as={Col} controlId= "dateDeparture">
			 									<Form.Label>Departure Date</Form.Label>
														<Form.Control 
							 								type = 'date'
							 								//value = {dateDeparture}
							 								onChange = {e => setNewDateDeparture(e.target.value)}
							 								required
														/>
													</Form.Group>

											<Form.Group as={Col} controlId= "dateArrival">
													 <Form.Label>Arrival Date</Form.Label>
														<Form.Control 
							 								type = 'date'
							 								//value = {dateArrival}
							 								onChange = {e => setNewDateArrival(e.target.value)}
							 								
														/>
													</Form.Group>
										</Row>

										<Row className="mb-3">
										    <Form.Group as={Col} controlId="adultpax">
										      <Form.Label>Adult</Form.Label>
										      <Form.Control 
										      	type="number" 
										      	placeholder="1" 
										      	//value = {adult}
										      	onChange = {e => setNewAdult(e.target.value)}
										      />
										    </Form.Group>

										    <Form.Group as={Col} controlId="childpax">
										      <Form.Label>Child</Form.Label>
										      <Form.Control 
										      	type="number" 
										      	placeholder="0" 
										      	//value = {child}
										      	onChange = {e => setNewChild(e.target.value)}
										      	/>
										    </Form.Group>

										     <Form.Group as={Col} controlId="infant">
										      <Form.Label>Infant</Form.Label>
										      <Form.Control 
										      	type="number" 
										      	placeholder="0" 
										      	//value = {infant}
										      	onChange = {e => setNewInfant(e.target.value)}

										      	/>
										    </Form.Group>
										  </Row> 						
							</Modal.Body>

							 <Modal.Footer>
							 	<Button variant="secondary" onClick={closeEdit}>Close</Button>
					            <Button variant="secondary" type="submit">Submit</Button>
					        </Modal.Footer>

						</Form>
						</Modal>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}