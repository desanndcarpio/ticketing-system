import {useState, useContext, Fragment} from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
//import {Navigate, useNavigate, Link} from 'react-router-dom'
//import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function AddProfile(){

	const {user, setUser} = useContext(UserContext)

	//const [userId, setUserId] = useState('')
	const [name, setName] = useState('')
	const [gender, setGender] = useState('')
	const [birthday, setBirthday] = useState('')
	const [age,setAge] = useState('')
	const [address, setAddress] = useState('')
	const [email, setEmail] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [passportNo, setPassportNo] = useState('')

/*	const retrieveUserDetails = (token) =>{
		fetch(`http://localhost:4000/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})

			setUserId(data._id)
		})
	}*/

	function addProfile(e) {
		e.preventDefault();

		fetch(`https://airline-ticketing-api.onrender.com/clientProfiles/add`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user._id,
				name : name,
				gender : gender,
				birthday : birthday,
				age : age,
				address : address,
				email : email,
				mobileNo : mobileNo,
				passportNo : passportNo
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data)

			alert('ok')
		})
		
		setName('')
		setGender('')
		setBirthday('')
		setAge('')
		setAddress('')
		setEmail('')
		setMobileNo('')
		setPassportNo('')

	}

	return(
		<Fragment>
			<h1 className="text-center mt-5">Add Passenger</h1>
			<Row className="justify-content-center mt-5">
				<Col md={6} >
					<Form onSubmit={(e) => addProfile(e)}>
						<Form.Group>
							<Form.Label>Full Name</Form.Label>
								<Form.Control 
				 					type = 'name'
				 					value = {name}
				 					onChange = {e => setName(e.target.value)}
				 					required
								/>
						</Form.Group>

						<Row className="mb-3"> 
						<Form.Group  as={Col} >
							<Form.Label>Gender</Form.Label>
								<Form.Control 
									as = "select"
									type = 'gender'
				 					value = {gender}
				 					onChange = {e => setGender(e.target.value)}
				 					required
								>
									<option value="">Select Gender</option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</Form.Control>
						</Form.Group>
						

						<Form.Group  as={Col}>
							<Form.Label>Birthday</Form.Label>
								<Form.Control 
				 					type = 'date'
				 					value = {birthday}
				 					onChange = {e => setBirthday(e.target.value)}
				 					required
								/>

						</Form.Group>

						<Form.Group  as={Col}>
							<Form.Label>Age</Form.Label>
								<Form.Control 
				 					type = 'number'
				 					value = {age}
				 					onChange = {e => setAge(e.target.value)}
				 					required
								/>
						</Form.Group>

						</Row>


						<Form.Group>
							<Form.Label>Complete Address*</Form.Label>
								<Form.Control 
				 					type = 'address'
				 					value = {address}
				 					onChange = {e => setAddress(e.target.value)}
								/>
						</Form.Group>
						
						<Form.Group>
							<Form.Label>Email*</Form.Label>
								<Form.Control 
				 					type = 'email'
				 					value = {email}
				 					onChange = {e => setEmail(e.target.value)}
								/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Mobile No.*</Form.Label>
								<Form.Control 
				 					type = 'mobile'
				 					value = {mobileNo}
				 					onChange = {e => setMobileNo(e.target.value)}
								/>
						</Form.Group>


						<Form.Group>
							<Form.Label>Passport Number</Form.Label>
								<Form.Control 
				 					type = 'passportNo'
				 					value = {passportNo}
				 					onChange = {e => setPassportNo(e.target.value)}
				 					required
								/>
						</Form.Group>
						<h6><em>* Optional</em></h6>
						<Button  variant = 'secondary' type= 'submit' id = 'submitBtn' className= "mt-4">Submit</Button>
					</Form>
				</Col>
			</Row>	
		</Fragment>
	)
}