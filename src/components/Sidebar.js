import {Fragment , useContext} from 'react'
import {Button, ButtonGroup, DropdownButton, Dropdown} from 'react-bootstrap'
import {Link} from 'react-router-dom' 
import UserContext from '../UserContext'

import '../App.css'


export default function Sidebar(){

  const {user} = useContext(UserContext)

/*  return(
    <Fragment>
      <ProSidebar id="sidebar">
        <Menu iconShape="square">
          <MenuItem >Dashboard</MenuItem>
          <SubMenu title="Components">
            <MenuItem>Component 1</MenuItem>
            <MenuItem>Component 2</MenuItem>
          </SubMenu>
        </Menu>
      </ProSidebar>
    </Fragment>
  )*/

  return(
    <Fragment>
      <div className="Sidebar">
         <ButtonGroup vertical>
          <h3 className="m-3 mt-5">Dashboard</h3>

            {(user.role === 'staff') ?
              <Fragment>
                <Button variant="outline-light" className="m-4" as={Link} to="/BInProgress" >In Progress</Button>
                <Button variant="outline-light" className="m-4" as={Link} to="/BPending">Pending</Button>

                <Button variant="outline-light" className="m-4" as={Link} to="/BReqRefund">Request Refund</Button>
                <Button variant="outline-light" className="m-4" as={Link} to="/BRefund">Refund</Button>
                <Button variant="outline-light" className="m-4" as={Link} to="/BSuccess">Success</Button>
                <Button variant="outline-light" className="m-4" as={Link} to="/BCancelled">Cancelled</Button>
              </Fragment>
              
              : (user.role === 'manager') ?
                <Fragment>
                  <h5>Staff Incident Tickets</h5>
                  <Button variant="outline-light" className="m-3" as={Link} to="/SUrgent">Urgent Tickets</Button>
                  <Button variant="outline-light" className="m-3" as={Link} to="/SAddCredits">Add Credits</Button>
                  <Button variant="outline-light" className="m-3" as={Link} to="/SMaintenance">Maintenance</Button>
                  <Button variant="outline-light" className="m-3" as={Link} to="/SDBError">DB Error</Button>
                  <Button variant="outline-light" className="m-3" as={Link} to="/SOthers">Other Concerns</Button>
                  <Button variant="outline-light" className="m-3" as={Link} to="/SResolved">Resolved</Button>
                  <Button variant="outline-light" className="m-3" as={Link} to="/SDeclined">Declined</Button>

                  <h5>Buyer Inquiry Tickets</h5>
                  <Button variant="outline-light" className="m-3" as={Link} to="/BForApproval">For Approval</Button>
                  <Button variant="outline-light" className="m-3" as={Link} to="/BRefund">Refund</Button>
                  <Button variant="outline-light" className="m-3" as={Link} to="/BSuccess">Success</Button>
                  <Button variant="outline-light" className="m-3" as={Link} to="/BCancelled">Cancelled</Button>
                </Fragment>

              : (user.role === 'it') ?
                <Fragment>
                  <Button variant="outline-light" className="m-4" as={Link} to="/SITickets" >Incident Tickets</Button>
                  <Button variant="outline-light" className="m-4" as={Link} to="/SResolved">Resolved</Button>
                  <Button variant="outline-light" className="m-4" as={Link} to="/SUnresolved">Unresolved</Button>
                </Fragment>

              : (user.role === 'admin') ?
                <Fragment>
                  <Button variant="outline-light" className="m-4" as={Link} to="/Users" >Users</Button>
                  <Button variant="outline-light" className="m-4" as={Link} to="/AllBTickets">Buyer Inquiry Tickets</Button>
                  <Button variant="outline-light" className="m-4" as={Link} to="/AllSTickets">Staff Incident Tickets</Button>
                </Fragment>
              : null
            }


        </ButtonGroup>
      </div>
    </Fragment>
  )
}

