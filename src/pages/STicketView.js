import {useState, useEffect, useContext, Fragment} from 'react'
import {Container, Row, Col, Card, Button, Table, Accordion, Modal, Form} from 'react-bootstrap'
import {useParams , useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function STicketView() {

	const {ticketId} = useParams()

	const {user} = useContext(UserContext)

	const history = useNavigate()

	const [approval, setApproval] = useState('')
	const [concern, setConcern] = useState('')
	const [subject, setSubject] = useState('')
	const [createdOn, setCreatedOn] = useState('')
	const [messages, setMessages] = useState([])
	const [status, setStatus] = useState('')
	const [userId, setUserId] = useState('')

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${ticketId}`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data)
			setApproval(data.approval)
			setConcern(data.concern)
			setSubject(data.subject)
			setCreatedOn(data.createdOn)
			setMessages(data.messages)
			setStatus(data.status)
			setUserId(data.userId)
		})
	}, [ticketId])

	function renderMessages (messages) {
		return messages.map(message => {
			//console.log(message)
			return (
				<tr>
					<td>{message.sender.name} : {message.message} <br/>Date Posted: {new Date(message.postedOn).toLocaleDateString()}</td>
					<hr/>
				</tr>
			)
		})
	}

	function addMessage(e){
		let {value: newMessage} = Swal.fire({
			title: 'Add Message',
			input: 'text',
			showCancelButton: true,
			preConfirm: (newMessage) => {
				//console.log(newMessage)
				if(newMessage){
					fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${ticketId}/addMessage`, {
						method: 'POST',
						headers: {
							"Content-Type" : "application/json",
							Authorization : `Bearer ${localStorage.getItem("token")}`
						},
						body: JSON.stringify({
							message: newMessage
						})
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						history(0)
					})
				}
			}
		})
	}

	function approve(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${ticketId}/approval`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				approval: 'approved'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title:'Ticket Approved',
				  icon:'success'
			})
			history(0)
		})
	}

	function decline(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${ticketId}/approval`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				approval: 'declined'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title: 'Ticket Declined',
				  icon: 'success'
			})
		history(0)	
		})
	}

	function resolved(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${ticketId}/status`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: 'resolved'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title:'Ticket Resolved',
				  icon:'success'
			})
			history(0)
		})
	}

	function unresolved(e){
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/${ticketId}/status`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: 'unresolved'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title: 'Ticket Unresolved',
				  icon: 'success'
			})
		history(0)	
		})
	}

	

	return(
		<Fragment>
			<Container>
				<Row className="m-5">
					<Col className="mt-5">
						<h5>Control Number: {ticketId}</h5>
						<h5>Staff Name: {userId.name}</h5>
						<h5>Email: {userId.email}</h5>
						<h6>Created on: {new Date(createdOn).toLocaleDateString()}</h6>
						<Table striped bordered hover>
							<thead>
								<tr>
									<th>Concern</th>
									<th>Subject</th>
									<th>Status</th>
									<th>Approval</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td>{concern}</td>
									<td>{subject}</td>
									<td>{status}</td>
									<td>{approval}</td>
									<td>
										
										{(user.role === 'manager') ? 
											<Fragment>
												<Button variant="secondary" className="mx-2" onClick={(e) => addMessage(e)}>Add Message</Button>
												<Button variant="secondary" className="mx-2" onClick={(e) => approve(e)}>Approve</Button> 
												<Button variant="secondary" className="mx-2" onClick={(e) => decline(e)}>Decline</Button> 
											</Fragment>

										: (user.role === 'staff') ?
											<Fragment>
												<Button variant="secondary" className="mx-2" onClick={(e) => addMessage(e)}>Add Message</Button>
											</Fragment>

										: (user.role === 'it') ?
											<Fragment>
												<Button variant="secondary" className="mx-2" onClick={(e) => addMessage(e)}>Add Message</Button>
												<Button variant="secondary" className="mx-2" onClick={(e) => resolved(e)}>Resolved</Button> 
												<Button variant="secondary" className="mx-2" onClick={(e) => unresolved(e)}>Unresolved</Button>
											</Fragment>

										: (user.role === 'admin') ?
											<Fragment>
												<Button variant="secondary" className="mx-2" onClick={(e) => addMessage(e)}>Add Message</Button>
												<Button variant="secondary" className="mx-2" onClick={(e) => approve(e)}>Approve</Button> 
												<Button variant="secondary" className="mx-2" onClick={(e) => decline(e)}>Decline</Button> 
												<Button variant="secondary" className="mx-2" onClick={(e) => resolved(e)}>Resolved</Button> 
												<Button variant="secondary" className="mx-2" onClick={(e) => unresolved(e)}>Unresolved</Button>
											</Fragment>
										: null										
									}
									</td>
								</tr>
							</tbody>
						</Table>
					
					<Accordion className="mb-3">
						<Accordion.Item eventKey="0">
							<Accordion.Header>Messages</Accordion.Header>
							<Accordion.Body>
								{renderMessages(messages)}
							</Accordion.Body>
						</Accordion.Item>
					</Accordion>

					</Col>
				</Row>
				<Button variant="secondary" onClick={()=>history(-1)}>Go Back</Button>
			</Container>	
		</Fragment>
	)
}