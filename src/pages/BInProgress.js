import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import InProgressCard from '../components/InProgressCard'

export default function BInProgress(){

	const {user, setUser} = useContext(UserContext)

	const [BInProgress, setBInProgress] = useState([])


/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/

/*	useEffect(() => {
		fetch(`https://secure-plateau-49977.herokuapp.com/buyerTickets/inprogress`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setBInProgress(data.map(BInProgress => {
				return(
					<Fragment>
						<InProgressCard key = {BInProgress.id} BInProgressProp= {BInProgress}/>
					</Fragment>
				)
			}))
		})
	}, [])

	//console.log(BInProgress.length)


	return(
		<Fragment>
			<Container>
				<Row>
					<Col>
						{(BInProgress.length === 0) ? <h1 className="text-center m-5">NO TICKETS</h1> : BInProgress}
					</Col>
				</Row>
			</Container>	
		</Fragment>
	)*/

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/inprogress`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => {
			//console.log(res)
			return res.json()})
		.then(data => {
			//console.log('aaaaa')
			setBInProgress(data)
		})
	}, [])

	function renderBInProgress(BInProgress) {
		return BInProgress.map(i => {
			return(
				<tr>
					<td><Link to = {`/buyerTickets/${i._id}`}>{i._id}</Link></td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.buyer.name}</td>
					<td>{i.buyer.email}</td>
				</tr>				
			)
		})
	}

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9} >
						<Container>
							<Row className="m-5">
								<Col className="mt-5">
									<h1>In Progress</h1>
									<p>As of: {new Date().toLocaleString() + ""}</p>
									<Table striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Date Created</th>
												<th>Buyer Name</th>
												<th>Buyer Email</th>
											</tr>
										</thead>

										<tbody>
											{(BInProgress.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderBInProgress(BInProgress)}
										</tbody>							
									</Table>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}