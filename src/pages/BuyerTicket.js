import {Fragment, useContext} from 'react'
//import {Link} from 'react-router-dom'
import {Tabs, Tab, Container, Row} from 'react-bootstrap'
//import UserContext from '../UserContext'

import BuyerAddTicket from '../components/BuyerAddTicket'
import ViewBuyerTicket from '../components/ViewBuyerTicket'

export default function BuyerTicket(){

	//const {user, setUser} = useContext(UserContext)

/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role

			})
		})
	}
*/
		return(
		<Fragment>
			<Container>
			<Row className="m-0" >
				<Tabs className="mt-5 tabs-link" tabclassname="text-secondary">
				
					<Tab eventKey="ticketView" title="View Ticket">
						<Container>
							<Row className="mb-5">
		    					<ViewBuyerTicket/>
		    				</Row>
		    			</Container>
		  			</Tab>

					 <Tab eventKey="addTicket" title="Add Ticket">
					 	<Container>	
					 		<Row className="mb-5 ">
					    		<BuyerAddTicket/>
					    	</Row>
					    </Container>
					 </Tab>

				 </Tabs>
				 </Row>
			</Container>
		</Fragment>	
	)
}
