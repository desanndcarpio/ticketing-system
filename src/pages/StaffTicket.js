import {Fragment, useContext} from 'react'
import {Tabs, Tab, Container, Row} from 'react-bootstrap'
import UserContext from '../UserContext'

import StaffAddTicket from '../components/StaffAddTicket'
import ViewStaffTicket from '../components/ViewStaffTicket'

export default function StaffTicket(){

	const {user, setUser} = useContext(UserContext)

	const retrieveUserDetails = (token) =>{
		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			setUser({
				id: data._id,
				role: data.role

			})
		})
	}

		return(
		<Fragment>
			<Container>
			<Row className="m-0" >
				<Tabs className="mt-5 tabs-link" tabclassname="text-secondary">
				
					<Tab eventKey="ticketView" title="View Ticket">
						<Container>
							<Row className="mb-5">
		    					<ViewStaffTicket/>
		    				</Row>
		    			</Container>
		  			</Tab>

					 <Tab eventKey="addTicket" title="Add Ticket">
					 	<Container>	
					 		<Row className="mb-5 ">
					    		<StaffAddTicket/>
					    	</Row>
					    </Container>
					 </Tab>

				 </Tabs>
				 </Row>
			</Container>
		</Fragment>	
	)
}
