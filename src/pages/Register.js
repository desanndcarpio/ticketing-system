import {useState, useEffect, useContext, Fragment} from 'react'
import {Row, Col, Form, Button} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Register(){

	const {user, setUser} = useContext(UserContext)

	const history = useNavigate()

	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [password2, setPassword2] = useState('')
	const [isActive, setIsActive] = useState(false)

	function registerUser(e){

		e.preventDefault();

		fetch(`https://airline-ticketing-api.onrender.com/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				name : name,
				email: email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			if(data === true){
				Swal.fire({
					title: 'Registration Successful',
					icon: 'success',
					text: 'Welcome'
				})

				history('/login')

			} else {
				Swal.fire({
					title: 'Duplicate email Found',
					icon: 'error',
					text: 'Please provide a different email'
				})
			}
		})

		setName('');
		setEmail('');
		setPassword('');
		setPassword2('');
	}

	const retrieveUserDetails = (token) => {

		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role

			})
		})

	}

	useEffect (() => {
		if((email !== '' && password !== '' && password2 !== '' && name !== '') && (password === password2)){
					setIsActive(true)	
			} else {
					setIsActive(false)
			}	

	}, [name, email, password, password2])

	return(

	/*	(user._id !== null) ?
			<Navigate to="/" />
		:
*/		<Fragment>
			<h1 className="text-center m-5">Register</h1>
			<Row className="m-5 my-auto mb-5" style={{height: "603px"}}>
				<Col md={4} className= "mx-auto">
					<Form onSubmit={(e)=>registerUser(e)}>
						<Form.Group  controlId= "name" className="my-3">
						<Form.Label>Name:</Form.Label>
						<Form.Control
							type = 'name'
							placeholder = 'Please enter your full name here'
						 	value = {name} /*getter, the input*/
							onChange = {e => setName(e.target.value)} 
							required
						/>
						</Form.Group>

						

						<Form.Group  controlId= "email" className="my-2">
							<Form.Label>Email Address:</Form.Label>
							<Form.Control
								type = 'email'
								placeholder = 'your-email@mail.com'
							 	value = {email}
								onChange = {e => setEmail(e.target.value)} 
								required
							/>
							<Form.Text className = "text-muted">
								We'll never share your email with anyone else. 
							</Form.Text>	
						</Form.Group>

				
						
						<Form.Group controlId = "password" className="my-2">
							<Form.Label>Password:</Form.Label>
							<Form.Control
								type = "password"
								value = {password}
								onChange = {e => setPassword(e.target.value)}
								required
							/>
						</Form.Group>

						<Form.Group controlId = "password2" className="my-3">
							<Form.Label>Verify Password:</Form.Label>
							<Form.Control
								type = 'password'
								placeholder = 'Please verify your password'
								value = {password2}
								onChange = {e => setPassword2(e.target.value)}
								required
							/>
						</Form.Group>
						<div className="my-4">
							{ isActive ? 
							<Button variant = 'secondary' type= 'submit' id = 'submitBtn'>
								Register
							</Button>

							: 
							<Button variant = 'secondary' type= 'submit' id = 'submitBtn' disabled>
								Register
							</Button>
						}
						</div>
						
					</Form>

				</Col>
			</Row>
		</Fragment>
		
	)
}