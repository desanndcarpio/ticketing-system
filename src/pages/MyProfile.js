import {Fragment, useState, useEffect} from 'react'
import {Container, Row, Col, Table} from 'react-bootstrap'
import UserContext from '../UserContext'

export default function MyProfile(){

	const [user, setUser] = useState('')

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUser(data)
		})
	},[])

	//console.log(user)

	return(
		<Fragment>
			<Container>
				<h1 className= "m-5">My Profile</h1>
				<Row className="mx-auto justify-content-center mt-5">
					<Col md={8}>
						<Table>
							<tbody>
								<tr>
									<th width="400px">ID Number</th>
									<td>{user._id}</td>
								</tr>

								<tr>
									<th>Name</th>
									<td>{user.name}</td>
								</tr>

								<tr>
									<th>Email</th>
									<td>{user.email}</td>
								</tr>
								{(user.role === 'user') ? null :

								<tr>
									<th>Position</th>
									<td>{user.role}</td>
								</tr>
								}

							</tbody>
						</Table>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
} 