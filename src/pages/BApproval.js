import {Fragment, useContext, useState, useEffect} from 'react'
//import {Link} from 'react-router-dom'
import {Col, Row, Container} from 'react-bootstrap'
import UserContext from '../UserContext' 

import ApprovalCard from '../components/ApprovalCard'

export default function BApproval(){

	const {user, setUser} = useContext(UserContext)

	const [BApproval, setBApproval] = useState([])


/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}
*/
	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/approval`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setBApproval(data.map(BApproval => {
				return(
					<Fragment>
						<ApprovalCard key = {BApproval.id} BApprovalProp= {BApproval}/>
					</Fragment>
				)
			}))
		})
	}, [])

	return(
		<Fragment>
			<Container>
				<Row className="m-5">
					<Col className="mt-5">
						{(BApproval.length === 0) ? <h1 className="text-center m-5">NO TICKETS</h1> : BApproval}
					</Col>
				</Row>
			</Container>	
		</Fragment>
	)

}