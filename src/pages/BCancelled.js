import {Fragment, useContext, useState, useEffect} from 'react'
//import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import CancelledCard from '../components/CancelledCard'

export default function BCancelled(){

	const {user, setUser} = useContext(UserContext)

	const [BCancelled, setBCancelled] = useState([])


/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/cancelled`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setBCancelled(data.map(BCancelled => {
				return(
					<Fragment>
						<CancelledCard key = {BCancelled.id} BCancelledProp= {BCancelled}/>
					</Fragment>
				)
			}))
		})
	}, [])

		return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<Col className="mt-5">
									<h1>Cancelled Tickets</h1>
									<p>As of: {new Date().toLocaleString() + ""}</p>
									<Table striped bordered hover>
										<thead>
												<tr>
													<th>Control Number</th>
													<th>Buyer Name</th>
													<th>Email</th>
													<th>Airline</th>
													<th>Origin</th>
													<th>Destination</th>
													<th>Total Amount</th>
													<th>Status</th>					
												</tr>
										</thead>

										<tbody>
											{(BCancelled.length === 0) ? <h1 className="text-center m-5">NO TICKETS</h1> : BCancelled}
										</tbody>	
									</Table>
									
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>	
		</Fragment>
	)

}