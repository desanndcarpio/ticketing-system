import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Container, Row, CardGroup, Card, Button, Col} from 'react-bootstrap'
import UserContext from '../UserContext'

import Sidebar from '../components/Sidebar'

export default function MDashboard(){

	const {user, setUser} = useContext(UserContext)

	const [noUrgent, setNoUrgent] = useState('0')
	const [noAddCredits, setNoAddCredits] = useState('0')
	const [noMaintenance, setNoMaintenance] = useState('0')
	const [noDBError, setNoDBError] = useState('0')
	const [noOthers,setNoOthers] = useState('0')

	const [noResolved, setNoResolved] = useState('0')
	const [noDeclined, setNoDeclined] = useState('0')

	const [noRefund, setNoRefund] = useState('0')
	const [noSuccess, setNoSuccess] = useState('0')
	const [noCancelled, setNoCancelled] = useState('0')

	const [noPending, setNoPending] = useState('0')


	const retrieveUserDetails = (token) =>{
		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role

			})
		})
	}

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/urgentTickets`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setNoUrgent(data.length)
		})
	},[])

	// console.log(noUrgent)



	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/addCredits`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setNoAddCredits(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/maintenance`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setNoMaintenance(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/dBError`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setNoDBError(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/others`,{
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setNoOthers(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/forApproval`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> {
			return res.json()
		})
		.then(data => {
			setNoPending(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/refund`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data.length)

			setNoRefund(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/success`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data.length)

			setNoSuccess(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/cancelled`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data.length)

			setNoCancelled(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/resolved`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setNoResolved(data.length)
		})
	}, [])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/declined`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setNoDeclined(data.length)
		})
	})



	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row>
								<h1 className="mt-5">Staff Tickets</h1>
								<Col md={12} className="text-center m-5">
									
									<Card border="danger">
										<Card.Body>
											<Card.Title>Urgent</Card.Title>
											<Card.Title>{noUrgent}</Card.Title>
											<Button variant="secondary" as={Link} to="/SUrgent">Go</Button>
										</Card.Body>
									</Card>

									<CardGroup>
										<Card>
											<Card.Body>
												<Card.Title>Add Credits</Card.Title>
												<Card.Title>{noAddCredits}</Card.Title>
												<Button variant="secondary" as={Link} to="/SAddCredits">Go</Button>
											</Card.Body>
										</Card>
										
										<Card>
											<Card.Body>
												<Card.Title>Maintenance</Card.Title>
												<Card.Title>{noMaintenance}</Card.Title>
												<Button variant="secondary" as={Link} to="/SMaintenance">Go</Button>
											</Card.Body>
										</Card>

										<Card>
											<Card.Body>
												<Card.Title>DB Error</Card.Title>
												<Card.Title>{noDBError}</Card.Title>
												<Button variant="secondary" as={Link} to="/SDBError">Go</Button>
											</Card.Body>
										</Card>

										<Card>
											<Card.Body>
												<Card.Title>Other Concerns</Card.Title>
												<Card.Title>{noOthers}</Card.Title>
												<Button variant="secondary" as={Link} to="/SOthers">Go</Button>
											</Card.Body>
										</Card>
									</CardGroup>
								</Col>
							</Row>

							<Row>
								<Col md={12}  className="text-center m-5">
									<CardGroup>
										<Card>
											<Card.Body>
												<Card.Title>Resolved</Card.Title>
												<Card.Title>{noResolved}</Card.Title>
												<Button variant="secondary" as={Link} to="/SResolved">Go</Button>
											</Card.Body>
										</Card>

										<Card>
											<Card.Body>
												<Card.Title>Declined</Card.Title>
												<Card.Title>{noDeclined}</Card.Title>
												<Button variant="secondary" as={Link} to="/SDeclined">Go</Button>
											</Card.Body>
										</Card>
									</CardGroup>
								</Col>
							</Row>

							<Row>
								<h1 className="mt-5">Buyer Tickets</h1>
								<Col md={12} className="text-center m-5">
									<CardGroup>
										<Card>
											<Card.Body>
												<Card.Title>For Approval</Card.Title>
												<Card.Title>{noPending}</Card.Title>
												<Card.Text>Needs approval</Card.Text>
												<Button variant="secondary" as={Link} to="/BForApproval">Go</Button>
											</Card.Body>
										</Card>

										<Card>
												<Card.Body>
													<Card.Title>Refund</Card.Title>
													<Card.Title>{noRefund}</Card.Title>
													<Card.Text>tickets refunded</Card.Text>
													<Button  variant="secondary" as={Link} to="/BRefund">Go</Button>
												</Card.Body>
											</Card>

											<Card>
												<Card.Body>
													<Card.Title>Success</Card.Title>
													<Card.Title>{noSuccess}</Card.Title>
													<Card.Text>Sales Report</Card.Text>
													<Button  variant="secondary" as={Link} to="/BSuccess">Go</Button>
												</Card.Body>
											</Card>

											<Card>
												<Card.Body>
													<Card.Title>Cancelled</Card.Title>
													<Card.Title>{noCancelled}</Card.Title>
													<Card.Text>Tickets Cancelled</Card.Text>
													<Button  variant="secondary" as={Link} to="/BCancelled">Go</Button>
												</Card.Body>
											</Card>
									</CardGroup>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)

}