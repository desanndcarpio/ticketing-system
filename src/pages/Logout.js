import {useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	/*localStorage.clear();    --added a function unsetUser in App.js*/

	//Consume the UserContext object and desctructure it to access the user state and unsetUser function from the context provider
	const {unsetUser, setUser} = useContext(UserContext)

	//clear the localStorage of the user's information
	unsetUser();

	/*
		- placing the "setUser" setter function inside the useEffect is necessary because of the updates within ReactJS that a state of another component cannot be updated while trying to render a different component
		- by adding the useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of our user.
		-set the user state back to its original value
	*/
	//
	useEffect( () => {
		setUser({_id:null});
	})

	//Redirect allows us to navigate to a new location
	return(
		<Navigate to ='/login'/>
	)
}