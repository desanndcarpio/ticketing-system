import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import BForApprovalCard from '../components/BForApprovalCard' 

export default function BForApproval(){

	const {user, setUser} = useContext(UserContext)

	const [BForApproval, setBForApproval] = useState([])

/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/

/*	useEffect(()=> {
		fetch(`https://secure-plateau-49977.herokuapp.com/buyerTickets/forApproval`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data => {
			setBForApproval(data.map(BForApproval => {
				return(
					<Fragment>
						<BForApprovalCard key = {BForApproval.id} BForApprovalProp = {BForApproval}/>
					</Fragment>
				)
			}))
		})
	},[])



	return(
		<Fragment>
			<Container>
				<Row className="m-5">
					<h1 className="mt-5">Inquiry tickets for approval</h1>
					<Col md={12} className="m-5">
						{(BForApproval.length === 0) ? <h1 className="text-center m-5">NO TICKETS</h1> : BForApproval}
					</Col>
				</Row>
			</Container>
		</Fragment>
	)*/

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/forApproval`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => {
			//console.log(res)
			return res.json()})
		.then(data => {
			//console.log('aaaaa')
			setBForApproval(data)
		})
	}, [])

	function renderBForApproval(BForApproval) {
		return BForApproval.map(i => {
			return(
				<tr>
					<td><Link to = {`/buyerTickets/${i._id}`}>{i._id}</Link></td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.buyer.name}</td>
					<td>{i.buyer.email}</td>
				</tr>				
			)
		})
	}

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<Col className="mt-5">
									<Table striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Date Created</th>
												<th>Buyer Name</th>
												<th>Buyer Email</th>
											</tr>
										</thead>

										<tbody>
											{(BForApproval.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderBForApproval(BForApproval)}
										</tbody>							
									</Table>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>	
			</Container>
		</Fragment>
	)	
}