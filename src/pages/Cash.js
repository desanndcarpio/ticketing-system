import {useState, useEffect, useContext, Fragment} from 'react';
import {Row, CardGroup,Card, Container, Col, Button} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Cash(){

	const {user, setUser} = useContext(UserContext)

	const history = useNavigate()


	const [name, setName] = useState('')
	const [email, setEmail] = useState('')

	const [buyerTicket, setBuyerTicket] = useState('')
	const [buyerTicketId, setBuyerTicketId] = useState('')

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setEmail(data.email)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/forPayment`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			data = data[0];

			//console.log(data)

			if(data._id !== undefined){
				setBuyerTicket(data)
				setBuyerTicketId(data._id)
			} else {
				setBuyerTicket('')
				setBuyerTicketId('')
			}
		})
	},[])

	return( 
		<Fragment>
			<Row className="text-center m-5" >
				<Col  md={8} className="mx-auto">
					<h1>CASH PAYMENT</h1>
					<p><strong>Go to the nearest branch with these following details</strong></p>
				
					<CardGroup className="mb-4">
						<Card>
							<Card.Body>
								<Card.Text className="mb-2">Customer Name:</Card.Text>
								<Card.Text className="mb-2">Email:</Card.Text>
								<Card.Text className="mb-2">Order Ticket Number:</Card.Text>
							</Card.Body>
						</Card>

						<Card>
							<Card.Body>
								<Card.Text className="mb-2">{name}</Card.Text>
								<Card.Text className="mb-2">{email}</Card.Text>
								<Card.Text className="mb-2">{buyerTicket._id}</Card.Text>
							</Card.Body>
						</Card>
					</CardGroup>
				</Col>
			</Row>
		</Fragment>
	)
}