import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import MaintenanceCard from '../components/MaintenanceCard' 

export default function SMaintenance(){

	const {user, setUser} = useContext(UserContext)

	const [SMaintenance, setSMaintenance] = useState([])
/*
	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}
*/
/*	useEffect(()=> {
		fetch(`https://secure-plateau-49977.herokuapp.com/staffTickets/maintenance`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data => {
			setSMaintenance(data.map(SMaintenance => {
				return(
					<Fragment>
						<MaintenanceCard key = {SMaintenance.id} SMaintenanceProp = {SMaintenance}/>
					</Fragment>
				)
			}))
		})
	},[])


	return(
		<Fragment>
			<Container>
				<Row>
					<h1 className="mt-5">Staff Maintenance Request</h1>
					<Col md={12} className="m-5">
						{(SMaintenance.length === 0) ? <h1 className="text-center m-5">NO TICKETS</h1> : SMaintenance}
					</Col>
				</Row>
			</Container>
		</Fragment>
	)*/

	useEffect(()=> {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/maintenance`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data =>{
			setSMaintenance(data)
		})
	},[])


	function renderSMaintenance(SMaintenance) {
		return SMaintenance.map(i => {
			return(
				<tr>
					<td><Link to = {`/staffTickets/${i._id}`}>{i._id}</Link></td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.concern}</td>
					<td>{i.userId.name}</td>
				</tr>				
			)
		})
	}
	
	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<h3>Maintenance</h3>
								<Col className="mt-5">
									<Table striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Date Created</th>
												<th>Concern</th>
												<th>Staff Name</th>
											</tr>
										</thead>

										<tbody>
											{(SMaintenance.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderSMaintenance(SMaintenance)}
										</tbody>							
									</Table>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>		
	)	
}