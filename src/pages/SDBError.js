import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import DBErrorCard from '../components/DBErrorCard' 

export default function SDBError(){

	const {user, setUser} = useContext(UserContext)

	const [SDBError, setSDBError] = useState([])

/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/
/*
	useEffect(()=> {
		fetch(`https://secure-plateau-49977.herokuapp.com/staffTickets/dBError`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data => {
			setSDBError(data.map(SDBError => {
				return(
					<Fragment>
						<DBErrorCard key = {SDBError.id} SDBErrorProp = {SDBError}/>
					</Fragment>
				)
			}))
		})
	},[])


	return(
		<Fragment>
			<Container>
				<Row>
					<h1 className="mt-5">Staff DB Error Request</h1>
					<Col md={12} className="m-5">
						{(SDBError.length === 0) ? <h1 className="text-center m-5">NO TICKETS</h1> : SDBError}
					</Col>
				</Row>
			</Container>
		</Fragment>
	)*/

	useEffect(()=> {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/dBError`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data =>{
			setSDBError(data)
		})
	},[])


	function renderSDBError(SDBError) {
		return SDBError.map(i => {
			return(
				<tr>
					<td><Link to = {`/staffTickets/${i._id}`}>{i._id}</Link></td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.concern}</td>
					<td>{i.userId.name}</td>
				</tr>				
			)
		})
	}

	//console.log(SAddCredits)

	
	
	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<h3>Database Error</h3>
								<Col className="mt-5">
									<Table striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Date Created</th>
												<th>Concern</th>
												<th>Staff Name</th>
											</tr>
										</thead>

										<tbody>
											{(SDBError.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderSDBError(SDBError)}
										</tbody>							
									</Table>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>	
			</Container>
		</Fragment>		
	)
}