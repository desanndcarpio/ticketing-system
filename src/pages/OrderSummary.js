import {useState, useEffect, useContext, Fragment} from 'react';
import {Row, CardGroup,Card, Container, Col, Button} from 'react-bootstrap';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';


export default function OrderSummary(){

	const {user, setUser} = useContext(UserContext)

	const history = useNavigate()

	const [name, setName] = useState('')
	const [email, setEmail] = useState('')

	const [buyerTicket, setBuyerTicket] = useState('')
	const [buyerTicketId, setBuyerTicketId] = useState('')
	const [passengers, setPassengers] = useState([])

	//console.log(passengers)

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setEmail(data.email)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/forPayment`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			data = data[0];

			//console.log(data)

			if(data._id !== undefined){
				setBuyerTicket(data)
				setBuyerTicketId(data._id)
				setPassengers(data.passengers)
			} else {
				setBuyerTicket('')
				setBuyerTicketId('')
				setPassengers('')
			}
		})
	},[])

	//console.log(buyerTicket)

	function MOP(e) {
		const inputOptions = new Promise((resolve)=> {
			setTimeout(() => {
				resolve({
					'Cash' : 'Cash',
					'OnlinePayment' : 'Card/Online Payment'
				})
			}, 1000)
		})

		const {value : mop} = Swal.fire({
			title: 'Select Mode of Payment',
			input: 'radio',
			inputOptions : inputOptions,
			inputValidator: (mop) => {
				//console.log(mop)
				if(!mop){
					return 'You need to choose Mode of Payment'
				} else {
					history(`/${mop}`)
				}
			}
		})

	}



	function renderPassengers(passengers){
		return passengers.map(passenger => {
			//console.log(passenger)
			return(
				<CardGroup className="mb-4">
					<Card>
						<Card.Body>
							<Card.Text className="mb-2">Passenger Name:</Card.Text>
							<Card.Text className="mb-2">Passport Number:</Card.Text>
							<Card.Text className="mb-2">Birthday:</Card.Text>
							<Card.Text className="mb-2">Gender:</Card.Text>
						</Card.Body>
					</Card>

					<Card>
						<Card.Body>
							<Card.Text className="mb-2">{passenger.passengerId.name}</Card.Text>
							<Card.Text className="mb-2">{passenger.passengerId.passportNo}</Card.Text>
							<Card.Text className="mb-2">{new Date(passenger.passengerId.birthday).toLocaleDateString()}</Card.Text>
							<Card.Text className="mb-2">{passenger.passengerId.gender}</Card.Text>
						</Card.Body>
					</Card>
				</CardGroup>
			)
		})
	}

	return(
		<Fragment>
			<Container>
				<Row>

					<Card.Footer className="mt-5">
						<small >PLEASE REVIEW ALL DETAILS</small>
					</Card.Footer>
					<Col md={8} className="mx-auto">
						<h1 className="m-4">Buyer Info</h1>

						<CardGroup className="mb-4">
							<Card>
								<Card.Body>
									<Card.Text className="mb-2">Customer Name:</Card.Text>
									<Card.Text className="mb-2">Email:</Card.Text>
								</Card.Body>
							</Card>

							<Card>
								<Card.Body>
									<Card.Text className="mb-2">{name}</Card.Text>
									<Card.Text className="mb-2">{email}</Card.Text>
								</Card.Body>
							</Card>
						</CardGroup>
					</Col>
				</Row>

				<Row>
					<Col md={8} className="mx-auto">
						<h1 className="m-4">Ticket Details</h1>
					
						<CardGroup  className="mb-4">
							<Card>
								<Card.Body>
									<Card.Text className="mb-2">Order Ticket Number:</Card.Text>
									<Card.Text className="mb-2">OneWay/Round Trip</Card.Text>
									<Card.Text className="mb-2">Airline</Card.Text>
									<Card.Text className="mb-2">Origin</Card.Text>
									<Card.Text className="mb-2">Destination</Card.Text>
									<Card.Text className="mb-2">Departure Date</Card.Text>
									{buyerTicket.isOneWay ? null : <Card.Text className="mb-2">Arrival Date</Card.Text>}
									{(buyerTicket.adult !== 0) ? <Card.Text className="mb-2">Adult</Card.Text> : null}
									{(buyerTicket.child !== 0) ? <Card.Text className="mb-2">Child</Card.Text> : null}
									{(buyerTicket.infant !== 0) ? <Card.Text className="mb-2">Infant</Card.Text> : null}	
									<Card.Text className="mb-2">Total Amount</Card.Text>							
								</Card.Body>
							</Card>

							<Card>
								<Card.Body>
									<Card.Text className="mb-2">{buyerTicket._id}</Card.Text>
									<Card.Text className="mb-2">{buyerTicket.isOneWay ? 'One Way' : 'Round Trip'} </Card.Text>
									<Card.Text className="mb-2">{buyerTicket.airline}</Card.Text>
									<Card.Text className="mb-2">{buyerTicket.origin}</Card.Text>
									<Card.Text className="mb-2">{buyerTicket.destination}</Card.Text>
									<Card.Text className="mb-2">{new Date(buyerTicket.dateDeparture).toLocaleDateString()}</Card.Text>
									{buyerTicket.isOneWay ? null : <Card.Text className="mb-2">{new Date(buyerTicket.dateArrival).toLocaleDateString()}</Card.Text>}
									{(buyerTicket.adult !== 0) ? <Card.Text className="mb-2">{buyerTicket.adult}</Card.Text> : null}
									{(buyerTicket.child !== 0) ? <Card.Text className="mb-2">{buyerTicket.child}</Card.Text> : null}
									{(buyerTicket.infant !== 0) ? <Card.Text className="mb-2">{buyerTicket.infant}</Card.Text> : null}	
									<Card.Text className="mb-2">{buyerTicket.totalAmount}</Card.Text>							
								</Card.Body>
							</Card>
						</CardGroup>
					</Col>
				</Row>	

				<Row>
					<Col md={8} className="mx-auto">
						<h1 className="m-4">Passenger Details</h1>
						{renderPassengers(passengers)}
					</Col>
				<Button variant="secondary" className="my-5" onClick={(e) => MOP(e)} >Proceed To Payment</Button>
				</Row>	

			</Container>
		</Fragment>
	)
}
