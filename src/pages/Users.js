import {Fragment, useContext, useState, useEffect} from 'react'
//import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import UsersCard from '../components/UsersCard'

export default function Users(){

	//const {user, setUser} = useContext(UserContext)

	const [Users, setUsers] = useState([])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/users/getUsers`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUsers(data.map(Users => {
				return(
					<Fragment>
						<UsersCard key = {Users.id} UsersProp = {Users}/>
					</Fragment>
				)
			}))
		})
	}, [])

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<h1 className="my-3">Users</h1>

								<Table>
									<thead>
										<tr>
											<th>User ID</th>
											<th>User Name</th>
											<th>Email</th>
											<th>Role</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>

									<tbody>
										{Users}
									</tbody>
								</Table>
							</Row>
						</Container>
					</Col>					
				</Row>
			</Container>
		</Fragment>
	)
}