import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Container, Row, CardGroup, Card, Button, Col} from 'react-bootstrap'
import UserContext from '../UserContext'

import Sidebar from '../components/Sidebar'
//import SApprovedCard from '../components/SApprovedCard'

export default function IDashboard(){

	const {user, setUser} = useContext(UserContext)

	const [noApproved, setNoApproved] = useState('0')
	const [noResolved, setNoResolved] = useState('0')
	const [noUnresolved, setNoUnresolved] = useState('0')
/*
	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role

			})
		})
	}*/

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/approved`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setNoApproved(data.length)
		})
	}, [])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/resolved`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setNoResolved(data.length)
		})
	}, [])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/unresolved`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setNoUnresolved(data.length)
		})
	})

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row>
								<h1 className="mt-5">Staff Incident Tickets</h1>
								<Col md={12} className="text-center m-5">
									<CardGroup>
										<Card>
											<Card.Body>
												<Card.Title>Incident Tickets</Card.Title>
												<Card.Title>{noApproved}</Card.Title>
												<Button variant="secondary" as={Link} to="/SITickets">Go</Button>
											</Card.Body>
										</Card>

										<Card>
											<Card.Body>
												<Card.Title>Resolved</Card.Title>
												<Card.Title>{noResolved}</Card.Title>
												<Button variant="secondary" as={Link} to="/SResolved">Go</Button>
											</Card.Body>
										</Card>

										<Card>
											<Card.Body>
												<Card.Title>Unresolved</Card.Title>
												<Card.Title>{noUnresolved}</Card.Title>
												<Button variant="secondary" as={Link} to="/SUnresolved">Go</Button>
											</Card.Body>
										</Card>
									</CardGroup>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)	
}