import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Container, Row, CardGroup, Card, Button, Col} from 'react-bootstrap'
//import { ProSidebar, Menu, MenuItem, SubMenu } from 'react-pro-sidebar';
//import 'react-pro-sidebar/dist/css/styles.css';
import UserContext from '../UserContext'

import Sidebar from '../components/Sidebar'

export default function SBDashboard(){

	const {user, setUser} = useContext(UserContext)

	const [noInProgress, setNoInProgress] = useState('0')
	const [noRevision, setNoRevision] = useState('0')
	const [noRequestRefund, setNoRequestRefund] = useState('0')
	const [noRefund, setNoRefund] = useState('0')
	const [noSuccess, setNoSuccess] = useState('0')
	const [noCancelled, setNoCancelled] = useState('0')
	//const [noApproval, setNoApproval] = useState('0')

	const retrieveUserDetails = (token) =>{
		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)

			setUser({
				id: data._id,
				role: data.role

			})
		})
	}

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/inprogress`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data.length)
			//console.log(data)
			setNoInProgress(data.length)
		})
	},[])

	//console.log(noInProgress)

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/revision`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data.length)
			//console.log(data)
			setNoRevision(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/get/allReqRefund`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setNoRequestRefund(data.length)
		})
	},[])


	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/refund`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data.length)

			setNoRefund(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/success`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data.length)

			setNoSuccess(data.length)
		})
	},[])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/ret/cancelled`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data.length)

			setNoCancelled(data.length)
		})
	},[])

/*


In progress - new request/on going
Pending - changes in ticket details
Request Refund - tickets request refund
Refund - tickets refunded
Success - Sales Report


*/


	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>
					<Col md={9}>
					<Container>
					<Row className="m-0">

					<Col md={12} className= "m-5 text-center">
					<h1 className="m-5 text-center">Buyer Inquiry Ticket Status</h1>
						<CardGroup>
							<Card 
								className="m-2"
								bg="light"
							>
								<Card.Header>In Progress</Card.Header>
								<Card.Body>
									<Card.Title>{noInProgress}</Card.Title>
									<Button variant="secondary" as={Link} to="/BInProgress" >Go</Button>
								</Card.Body>
							</Card>

							<Card 
								className="m-2"
								bg="light"
							>
								<Card.Header>Pending</Card.Header>
								<Card.Body>
									<Card.Title>{noRevision}</Card.Title>
									<Button  variant="secondary" as={Link} to="/BPending">Go</Button>
								</Card.Body>
							</Card>

							<Card
								className="m-2"
								bg="light"
							>
								<Card.Header>Request Refund</Card.Header>
								<Card.Body>
									<Card.Title>{noRequestRefund}</Card.Title>
									<Button  variant="secondary" as={Link} to="/BReqRefund">Go</Button>
								</Card.Body>
							</Card>
						</CardGroup>
					</Col>
				</Row>
				<Row>
					<Col md={12} className= "m-5 text-center">
						<CardGroup>
							<Card
								className="m-2"
								bg="light"
							>
								<Card.Header>Refund</Card.Header>
								<Card.Body>
									<Card.Title>{noRefund}</Card.Title>
									<Button  variant="secondary" as={Link} to="/BRefund">Go</Button>
								</Card.Body>
							</Card>

							<Card
								className="m-2"
								bg="light"
							>
								<Card.Header>Success</Card.Header>
								<Card.Body>
									<Card.Title>{noSuccess}</Card.Title>
									<Button  variant="secondary" as={Link} to="/BSuccess">Go</Button>
								</Card.Body>
							</Card>

							<Card
								className="m-2"
								bg="light"
							>
								<Card.Header>Cancelled</Card.Header>
								<Card.Body>
									<Card.Title>{noCancelled}</Card.Title>
									<Button variant="secondary" as={Link} to="/BCancelled">Go</Button>
								</Card.Body>
							</Card>
						</CardGroup>
					</Col>
				</Row>
					</Container>

					</Col>

				</Row>
				
			</Container>
		</Fragment>
	)

}

