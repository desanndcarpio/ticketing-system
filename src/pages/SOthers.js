import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import OthersCard from '../components/OthersCard' 

export default function SOthers(){

	const {user, setUser} = useContext(UserContext)

	const [SOthers, setSOthers] = useState([])

/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/

/*	useEffect(()=> {
		fetch(`https://secure-plateau-49977.herokuapp.com/staffTickets/others`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data => {
			setSOthers(data.map(SOthers => {
				return(
					<Fragment>
						<OthersCard key = {SOthers.id} SOthersProp = {SOthers}/>
					</Fragment>
				)
			}))
		})
	},[])


	return(
		<Fragment>
			<Container>
				<Row>
					<h1 className="mt-5">Staff Other Concerns Request</h1>
					<Col md={12} className="m-5">
						{(SOthers.length === 0) ? <h1 className="text-center m-5">NO TICKETS</h1> : SOthers}
					</Col>
				</Row>
			</Container>
		</Fragment>
	)*/

	useEffect(()=> {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/others`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data =>{
			setSOthers(data)
		})
	},[])


	function renderSOthers(SOthers) {
		return SOthers.map(i => {
			return(
				<tr>
					<td><Link to = {`/staffTickets/${i._id}`}>{i._id}</Link></td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.concern}</td>
					<td>{i.userId.name}</td>
				</tr>				
			)
		})
	}


	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<h3>Other Concerns</h3>
								<Col className="mt-5">
									<Table striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Date Created</th>
												<th>Concern</th>
												<th>Staff Name</th>
											</tr>
										</thead>

										<tbody>
											{(SOthers.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderSOthers(SOthers)}
										</tbody>							
									</Table>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>		
	)
}