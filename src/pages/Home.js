import {Fragment} from 'react'
import {Container, Row, Card, Col} from 'react-bootstrap'
//import {Link} from 'react-router-dom'
import '../App.css'
import HomeBanner from '../components/HomeBanner'


export default function Home(){

	return(
		<Fragment>		
			<HomeBanner/>	
		</Fragment>	
	)
}