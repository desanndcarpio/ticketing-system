import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'

export default function AllSTickets(){

	const {user, setUser} = useContext(UserContext)

	const [AllSTickets, setAllSTickets] = useState([])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/allTicket`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data => {
			setAllSTickets(data)
		})
	}, [])

	function renderAllSTickets(AllSTickets){
		return AllSTickets.map(i => {
			return(
				<tr>
					<td><Link to = {`/staffTickets/${i._id}`}>{i._id}</Link></td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.concern}</td>
					<td>{i.userId.name}</td>
				</tr>
			)
		})
	}

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<Col  className="mt-5">
									<h1>All Staff Tickets</h1>
									<Table striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Date Created</th>
												<th>Concern</th>
												<th>Staff Name</th>
											</tr>
										</thead>

										<tbody>
											{(AllSTickets.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderAllSTickets(AllSTickets)}
										</tbody>							
									</Table>
								</Col>
							</Row>	
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}