import {Fragment, useContext, useState, useEffect} from 'react'
//import {Link} from 'react-router-dom'
import {Col, Row, Container} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import UnresolvedCard from '../components/UnresolvedCard' 

export default function SUnresolved(){

	const {user, setUser} = useContext(UserContext)

	const [SUnresolved, setSUnresolved] = useState([])

/*		const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/unresolved` , {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setSUnresolved(data.map(SUnresolved => {
				return(
					<Fragment>
						<UnresolvedCard key = {SUnresolved.id} SUnresolvedProp = {SUnresolved}/>
					</Fragment>
				)
			}))
		})
	}, [])

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row>
								<h1 className="mt-5">Staff Unresolved Incident Tickets</h1>
								<Col md={12} className="m-5">
									{(SUnresolved.length === 0) ? <h1 className="text-center m-5">NO TICKETS</h1> : SUnresolved}
								</Col>
							</Row>	
						</Container>
					</Col>
				</Row>	
			</Container>
		</Fragment>
	)
}