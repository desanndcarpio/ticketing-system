import {Fragment, useContext} from 'react'
//import {Link} from 'react-router-dom'
import {Tabs, Tab, Container, Row, Col} from 'react-bootstrap'
import UserContext from '../UserContext'

import AddProfile from '../components/AddProfile'
import ViewProfile from '../components/ViewProfile'


export default function ClientProfile(){

	const {user, setUser} = useContext(UserContext)

	const retrieveUserDetails = (token) =>{
		fetch(`https://airline-ticketing-api.onrender.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}

	return(
		<Fragment>
			<Container>
			<Row className="m-0" >
				<Tabs className="mt-5 tabs-link" tabclassname="text-secondary">
				
					<Tab eventKey="profileView" title="View Passengers">
						<Container>
							<Row className="mb-5">
								<Col md={7} className="mx-auto">
		    						<ViewProfile/>
								</Col>
		    				</Row>
		    			</Container>
		  			</Tab>

					 <Tab eventKey="addProfile" title="Add Passenger">
					 	<Container>	
					 		<Row className="mb-5 ">
					    		<AddProfile/>
					    	</Row>
					    </Container>
					 </Tab>

				 </Tabs>
				 </Row>
			</Container>
		</Fragment>	
	)
}