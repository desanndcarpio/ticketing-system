import {Fragment, useContext, useState, useEffect} from 'react'
//import {Link} from 'react-router-dom'
import {Col, Row, Container} from 'react-bootstrap'
import UserContext from '../UserContext' 
import ReqRefundTicketCard from '../components/ReqRefundTicketCard'


export default function ReqRefundTicket(){

	const {user, setUser} = useContext(UserContext)

	const [reqRefundTicket, setReqRefundTicket] = useState([])
	


	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/myReqRefund`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setReqRefundTicket(data.map(reqRefundTicket=> {
				return(
					<Fragment>
						<ReqRefundTicketCard key= {reqRefundTicket.id} ReqRefundTicketProp= {reqRefundTicket}/>
					</Fragment>
				)
			}))
		})
	}, [])

	

	return(

		<Fragment>
			<Container>
				<Row>
					<Col>
						{reqRefundTicket}
					</Col>
				</Row>
			</Container>	
		</Fragment>

	)
}