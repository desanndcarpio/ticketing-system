import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'

export default function AllBTickets(){

	const {user, setUser} = useContext(UserContext)

	const [AllBTickets, setAllBTickets] = useState([])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/get/allTicket`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			//console.log(data)
			setAllBTickets(data)
		})
	},[])

	function renderAllBTickets(AllBTickets){
		return AllBTickets.map(i => {
			return(
				<tr>
					<td><Link to = {`/buyerTickets/${i._id}`}>{i._id}</Link></td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.buyer.name}</td>
					<td>{i.buyer.email}</td>
				</tr>
			)
		})
	}

	//console.log(AllBTickets)
	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<Col className="mt-5">
									<h1>Buyer Inquiry Tickets</h1>
									<Table striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Date Created</th>
												<th>Buyer Name</th>
												<th>Buyer Email</th>
											</tr>
										</thead>

										<tbody>
											{(AllBTickets.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderAllBTickets(AllBTickets)}
										</tbody>							
									</Table>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}