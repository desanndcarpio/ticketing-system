import {Fragment, useContext, useState, useEffect} from 'react'
//import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import DeclinedCard from '../components/DeclinedCard'

export default function SDeclined(){

	const {user, setUser} = useContext(UserContext)

	const [SDeclined, setSDeclined] = useState([])

/*	const retrieveUserDetails = (token) =>{
		fetch(`https://secure-plateau-49977.herokuapp.com/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				role: data.role
			})
		})
	}*/

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/declined`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=> res.json())
		.then(data => {
			setSDeclined(data.map(SDeclined => {
				return(
					<Fragment>
						<DeclinedCard key = {SDeclined.id} SDeclinedProp = {SDeclined}/>
					</Fragment>
				)
			}))
		})
	}, [])

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="mt-5">
								<Col md={12} className="m-5">
									<h1>Declined Staff Tickets</h1>
									<p>As of: {new Date().toLocaleString() + ""}</p>

									<Table striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Staff Name</th>
												<th>Date Posted</th>
												<th>Concern</th>
												<th>Subject</th>
												<th>Messages</th>
											</tr>
										</thead>

										<tbody>
											{(SDeclined.length === 0) ? <h1 className="text-center m-5">NO TICKETS</h1> : SDeclined}
										</tbody>
									</Table>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)

}