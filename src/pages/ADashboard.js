import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Container, Row, CardGroup, Card, Button, Col} from 'react-bootstrap'
import UserContext from '../UserContext'

import Sidebar from '../components/Sidebar'

export default function ADashboard(){

	const {user, setUser} = useContext(UserContext)

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-0">
								<Col md={12} className="text-center m-5">
								<h1 className="m-5">Admin Dashboard</h1>
									<CardGroup>
										<Card>
											<Card.Body>
												<Card.Title>Users</Card.Title>
												<Button variant="secondary" as={Link} to="/Users">Go</Button>
											</Card.Body>	
										</Card>

										<Card>
											<Card.Body>
												<Card.Title>Buyer Inquiry Tickets</Card.Title>
												<Button variant="secondary" as={Link} to="/AllBTickets">Go</Button>
											</Card.Body>	
										</Card>


										<Card>
											<Card.Body>
												<Card.Title>Staff Incident Tickets</Card.Title>
												<Button variant="secondary" as={Link} to="/AllSTickets">Go</Button>
											</Card.Body>	
										</Card>
									</CardGroup>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>	
		</Fragment>
	)
}