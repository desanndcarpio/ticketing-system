import {Fragment, useContext, useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import {Col, Row, Container, Table} from 'react-bootstrap'
import UserContext from '../UserContext' 

import Sidebar from '../components/Sidebar'
import UrgentCard from '../components/UrgentCard'

export default function SUrgents(){

	const {user, setUser} = useContext(UserContext)

	const [SUrgents, setSUrgents] = useState([])

	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/staffTickets/urgentTickets`, {
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setSUrgents(data)
		})
	},[])

	function renderSUrgents(SUrgents) {
		return SUrgents.map(i => {
			return(
				<tr>
					<td><Link to = {`/staffTickets/${i._id}`}>{i._id}</Link></td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.concern}</td>
					<td>{i.userId.name}</td>
				</tr>
			)
		})
	}

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<h3>Urgent Tickets</h3>
								<Col className="mt-5">
									<Table striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Date Created</th>
												<th>Concern</th>
												<th>Staff Name</th>
											</tr>
										</thead>

										<tbody>
											{(SUrgents.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderSUrgents(SUrgents)}
										</tbody>							
									</Table>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>		

	)
}