import {Fragment, useContext, useState, useEffect} from 'react'
import {Col, Row, Container, Table, Button, Modal, Accordion, Card, CardGroup} from 'react-bootstrap'
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext' 
import {Link} from 'react-router-dom'

import Sidebar from '../components/Sidebar'
//import ReqRefundCard from '../components/ReqRefundCard'

export default function BReqRefund(){

	const {user, setUser} = useContext(UserContext)

	const history = useNavigate()

	const [BReqRefund, setBReqRefund] = useState([])


	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/get/allReqRefund`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => {
			//console.log(res)
			return res.json()})
		.then(data => {
			//console.log('aaaaa')
			setBReqRefund(data)
		})
	}, [])


	
	function renderBReqRefund(BReqRefund) {
		return BReqRefund.map(i => {
			//console.log(i.airline)
			//console.log(ticketId)
			return(
				<tr>
					{/*<td><Button variant="link" onClick={(e)=> setTicketId(i._id)} onClick={() => setShow(true)}>{i._id}</Button></td>*/}
					<td><Link to = {`/buyerTickets/${i._id}`}>{i._id}</Link></td>
					<td>{new Date(i.createdOn).toLocaleDateString()}</td>
					<td>{i.buyer.name}</td>
					<td>{i.buyer.email}</td>
					
				</tr>
			)
		})
	}

	return(
		<Fragment>
			<Container className="m-0 p-0">
				<Row>
					<Col md={3} >
						<Sidebar/>
					</Col>

					<Col md={9}>
						<Container>
							<Row className="m-5">
								<Col className="mt-5">
									<h1>Request Refund</h1>
									<p>As of: {new Date().toLocaleString() + ""}</p>
									<Table  striped bordered hover>
										<thead>
											<tr>
												<th>Control Number</th>
												<th>Date Created</th>
												<th>Buyer Name</th>
												<th>Buyer Email</th>
											</tr>
										</thead>

										<tbody>
											{(BReqRefund.length === 0) ? <h1 className="m-5">NO TICKETS</h1> : renderBReqRefund(BReqRefund)}
										</tbody>
									</Table>
								</Col>
							</Row>
						</Container>
					</Col>
				</Row>
			</Container>
		</Fragment>
	)
}