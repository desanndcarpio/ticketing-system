import {useState, useEffect, useContext, Fragment} from 'react'
import {Container, Row, Col, Card, Button, Table, Accordion, Modal, Form} from 'react-bootstrap'
import {useParams , useNavigate, Link} from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function BTicketView() {

	const {ticketId} = useParams()

	const {user} = useContext(UserContext)

	const [show, setShow] = useState(false);

	const history = useNavigate()



	const [buyer, setBuyer] = useState('')
	const [isOneWay, setIsOneWay] = useState('')
	const [origin, setOrigin] = useState('')
	const [destination, setDestination] = useState('')
	const [airline, setAirline] = useState('')
	const [dateDeparture, setDateDeparture] = useState('')
	const [dateArrival, setDateArrival] = useState('')
	const [adult, setAdult] = useState('')
	const [child, setChild] = useState('')
	const [infant, setInfant] = useState('')
	const [passengers, setPassengers] = useState([])
	const [messages, setMessages] = useState([])
	const [createdOn, setCreatedOn] = useState('')
	const [status, setStatus] = useState('')
	const [approval, setApproval] = useState('')
	const [isPaid, setIsPaid] = useState('')
	const [availability, setAvailability] = useState('')
	const [totalAmount, setTotalAmount] = useState('')


	const [newIsOneWay, setNewIsOneWay] = useState('');
	const [newAirline, setNewAirline] = useState('');
	const [newOrigin, setNewOrigin] = useState('');
	const [newDestination, setNewDestination] = useState('');
	const [newDateDeparture, setNewDateDeparture] = useState('');
	const [newDateArrival, setNewDateArrival] = useState('');
	const [newAdult, setNewAdult] = useState('');
	const [newChild, setNewChild] = useState('0');
	const [newInfant, setNewInfant] = useState('0');

	const handleShow = () => setShow(true);
	const handleClose = () => setShow(false);


	//console.log(ticketId)
	useEffect(() => {
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${ticketId}`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			//data = data[0]
			//console.log(data)
			setBuyer(data.buyer)
			setIsOneWay(data.isOneWay)
			setOrigin(data.origin)
			setDestination(data.destination)
			setAirline(data.airline)
			setDateDeparture(data.dateDeparture)
			setDateArrival(data.dateArrival)
			setAdult(data.adult)
			setChild(data.child)
			setInfant(data.infant)
			setPassengers(data.passengers)
			setMessages(data.messages)
			setCreatedOn(data.createdOn)
			setStatus(data.status)
			setApproval(data.approval)
			setIsPaid(data.isPaid)
			setAvailability(data.availability)
			setTotalAmount(data.totalAmount)
		})
	}, [ticketId])


	function addMessage(e){
		let {value: newMessage} = Swal.fire({
			title: 'Add Message',
			input: 'text',
			showCancelButton: true,
			preConfirm: (newMessage) => {
				//console.log(newMessage)
				if(newMessage){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${ticketId}/addMessage`, {
						method: 'POST',
						headers: {
							'Content-Type' : 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							message: newMessage
						})
					})
					.then(res=> {
						return res.json()
					})
					.then(data => {
						history(0)
					})
				}
			}
		})
	}

	function changeStatus(e){
		const {value: newStatus} = Swal.fire({
			title: 'Select Status',
			input: 'select',
			inputOptions: {
				'inProgress' : 'In Progress',
				'revision' : 'Pending',
				'refund' : 'Refund',
				'success' : 'Success',
				'cancelled' : 'Cancelled'
			},
			inputPlaceholder: 'Select new Status',
			showCancelButton: true,
			inputValidator : (value) => {
				if(value){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${ticketId}/status`, {
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							status: value
						})
					})
					.then(res => {
						return res.json()
					})
					.then(data => {
						Swal.fire({
				  			title: 'Ticket Status Changed',
				 			icon: 'success'
						})
						history(0)
					})
				}
			}
		})
	}

	function forApproval(e){


		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${ticketId}/approval`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				approval: 'forApproval'
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			Swal.fire({
				  title: 'Ticket For Approval',
				  icon: 'success'
			})
			history(0)
		})
	}

	function renderMessages (messages) {
		return messages.map(message => {
			//console.log(message)
			return (
				<tr>
					<td>{message.sender.name} : {message.message} <br/>Date Posted: {new Date(message.postedOn).toLocaleDateString()}</td>
					<hr/>
				</tr>
			)
		})
	}

	//console.log(ticketId)

	function renderPassengers (passengers) {
		return passengers.map(passenger => {
			//console.log(passenger.passengerId.name)
			return (
				<li>{passenger.passengerId.name}</li>
			)
		})
	}


	function changeAvailability(e){
		let {value: newAvailability} = Swal.fire({
			title: 'Ticket Availablity',
			input: 'select',
			inputOptions : {
				'available': 'Available',
				'unavailable' : 'Unavailable',
				'waitListed' : 'Wait Listed'

			},
			inputPlaceholder: 'Select Availability',
			showCancelButton: true,
			inputValidator: (value) => {

				if(value){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${ticketId}/availability`,{
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							Authorization : `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							availability : value
						})
					})
					.then(res=> res.json())
					.then(data => {
						Swal.fire({
				  			title: 'Ticket Availablity Changed',
				 			icon: 'success'
						})
						history(0)
					})
				}
			}
		})
	}

	function addTotalAmount(e){
		let {value : totalAmount} = Swal.fire({
			title: 'Total Amount',
			input: 'text',
			showCancelButton: true,
			preConfirm : (value) => { 
				if(value){
					fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${ticketId}/addTotalAmount`, {
						method: 'PUT',
						headers: {
							'Content-Type' : 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},
						body: JSON.stringify({
							totalAmount: value
						})
					})
					.then(res=> res.json())
					.then(data => {
						Swal.fire({
				  			title: 'Total Amount Added',
				 			icon: 'success'
						})
						history(0)
					})
				}
			}
		})
	}

	const editTicket = (e, ticketId) => {
		e.preventDefault();
		

		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${ticketId}/details`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}, 
			body: JSON.stringify({
				isOneWay : newIsOneWay,
				airline : newAirline,
				origin : newOrigin,
				destination : newDestination,
				dateDeparture : newDateDeparture,
				dateArrival : newDateArrival,
				adult : newAdult,
				child: newChild,
				infant: newInfant
			})
		})
		.then(res=> {
			console.log(res)
			return res.json()
		})
		.then(data => {
			if(data) {

				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Ticket Details Updated'
				})
				history(0)
			} else {

				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Error'
				})
			}
		})

		setNewIsOneWay('')
		setNewAirline('')
		setNewDestination('')
		setNewDateDeparture('')
		setNewDateArrival('')
		setNewAdult('')
		setNewChild('')
		setNewInfant('')
	
	}

	function approve(e){
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${ticketId}/approval`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				approval: 'approved'
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
				  title:'Ticket Approved',
				  icon:'success'
			})
		history(0)	
		})
	}

	function decline(e){
		fetch(`https://airline-ticketing-api.onrender.com/buyerTickets/${ticketId}/approval`,{
			method: 'PUT', 
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				approval: 'declined'
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				  title: 'Ticket Declined',
				  icon: 'success'
			})
		history(0)	
		})
	}



	return(
		<Fragment>
			<Container>
				<Row className="m-5">
					<Col className="mt-5">
						<h5>Control Number: {ticketId}</h5>
						<h5>Customer: {buyer.name}</h5>
						<h6>Email: {buyer.email}</h6>
					<Table striped bordered hover>
						<thead>
							<tr>
								
								<th>  </th>
								<th>Airline</th>
								<th>Origin</th>
								<th>Destination</th>
								<th>Departure Date</th>
								{isOneWay ? null : <th>Arrival Date</th>}			
								{(adult !== 0) ? <th>Adult</th> : null}
								{(child !== 0) ? <th>Child</th> : null}
								{(infant !== 0) ? <th>Infant</th> : null}
								{(totalAmount !== '0') ? <th>Total Amount</th> : null}
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{isOneWay ? 'One Way' : 'Round Trip'}</td>
								<td>{airline}</td>
								<td>{origin}</td>
								<td>{destination}</td>
								<td>{new Date(dateDeparture).toLocaleDateString()}</td>
								{isOneWay ? null : <td>{new Date(dateArrival).toLocaleDateString()}</td>}
								{(adult !== 0) ? <td>{adult}</td> : null}
								{(child !== 0) ? <td >{child}</td> : null}
								{(infant !== 0) ? <td>{infant}</td>: null}
								{(totalAmount !== '0') ? <td>{totalAmount}</td> : null}
								
								
							</tr>
						</tbody>
					</Table>

					<Table>
						<thead>
							<tr>
								<th>Status</th>
								<th>Availablity</th>
								{(approval !== undefined) ? <th>Approval</th> : null }
								<th>Payment Status</th>
								{(status !== 'refund' && status !== 'cancelled' && status !== 'success') ? 

								<th>Actions</th>
								: null
								}
							</tr>
						</thead>

						<tbody>
							<tr>
								<td>{status}</td>
								<td>{availability}</td>
								{(approval !== undefined) ? <td>{approval}</td> : null}
								<td>{isPaid ? 'Paid' : 'Pending'}</td>
						
								<td>	
								{(status !== 'refund' && status !== 'cancelled' && status !== 'success' && user.role === 'staff') ? 

									<Fragment>
										<Button variant="secondary" className="m-2" onClick={(e) => addMessage(e)}>Add message</Button>
										<Button variant="secondary" className="m-2" onClick={(e) => changeStatus(e)}>Change Status</Button>
										<Button variant="secondary" className="m-2" onClick={(e) => forApproval(e)}>For Approval</Button>
										<Button variant="secondary" className="m-2" onClick={(e) => changeAvailability(e)}>Change Availability</Button>
										<Button variant="secondary" className="m-2" onClick={handleShow}>Change Details</Button>
										<Button variant="secondary" className="m-2" onClick={(e) => addTotalAmount(e)}>Add Total Amount</Button>
									</Fragment>

									: (status !== 'refund' && status !== 'cancelled' && status !== 'success' && user.role === 'manager') ?

									<Fragment>
										<Button variant="secondary" className="mx-2" onClick={(e) => approve(e)}>Approve</Button> 
										<Button variant="secondary" className="mx-2" onClick={(e) => decline(e)}>Decline</Button>
									</Fragment>
									
									: (user.role === 'admin') ?
										
										<Fragment>
											<Button variant="secondary" className="m-2" onClick={(e) => addMessage(e)}>Add message</Button>
											<Button variant="secondary" className="m-2" onClick={(e) => changeStatus(e)}>Change Status</Button>
											<Button variant="secondary" className="m-2" onClick={(e) => forApproval(e)}>For Approval</Button>
											<Button variant="secondary" className="m-2" onClick={(e) => changeAvailability(e)}>Change Availability</Button>
											<Button variant="secondary" className="m-2" onClick={handleShow}>Change Details</Button>
											<Button variant="secondary" className="m-2" onClick={(e) => addTotalAmount(e)}>Add Total Amount</Button>
											<Button variant="secondary" className="mx-2" onClick={(e) => approve(e)}>Approve</Button> 
											<Button variant="secondary" className="mx-2" onClick={(e) => decline(e)}>Decline</Button>
										</Fragment>

									: null

									}
								</td>
							</tr>
						</tbody>
					</Table>
					<Accordion className="mb-3">
						<Accordion.Item eventKey="0">
							<Accordion.Header>Messages</Accordion.Header>
							<Accordion.Body>
								{renderMessages(messages)}
							</Accordion.Body>
						</Accordion.Item>
					</Accordion>

					<Accordion>
						<Accordion.Item eventKey="0">
							<Accordion.Header>Passenger Details</Accordion.Header>
							<Accordion.Body>
								{(passengers.length !== 0) ? renderPassengers(passengers) : 'No Passenger Details Available'}
							</Accordion.Body>
						</Accordion.Item>
					</Accordion>
					<hr/>
					</Col>
				</Row>
				<Button variant="secondary" onClick={()=>history(-1)}>Go Back</Button>
						

					<Modal show={show} onHide={handleClose} size="lg" aria-labelledby="conatained-modal-title-vcenter" centered>
						<Form onSubmit={e => editTicket(e, ticketId)}>
							<Modal.Header closeButton>
								<Modal.Title  id="contained-modal-title-vcenter">EDIT TICKET DETAILS: {ticketId}</Modal.Title>
							</Modal.Header>

							<Modal.Body>
								<Form.Group>
										<Form.Control 
												as = "select"
												
							 					value = {newIsOneWay}
							 					onChange = {e => setNewIsOneWay(e.target.value)}
							 					required
											>
												<option value="">Select one</option>
												<option value="true">One Way</option>
												<option value="false">Round Trip</option>
											</Form.Control>
										</Form.Group>

										<Form.Group>
										<Form.Label>Airline</Form.Label>
											<Form.Control 
												as = "select"
				
							 					value = {newAirline}
							 					onChange = {e => setNewAirline(e.target.value)}
							 					required
											>
												<option value="">Select one</option>
												<option value="pal">PAL</option>
												<option value="ceb">CEB</option>
												<option value="air">AIR</option>
												<option value="delta">Delta</option>
											</Form.Control>
										</Form.Group>

			 							<Form.Group controlId= "origin">
			 								<Form.Label>Origin</Form.Label>
														<Form.Control 
							 								type = 'text'
							 								value = {newOrigin}
							 								onChange = {e => setNewOrigin(e.target.value)}
							 								required
														/>
										</Form.Group>

										<Form.Group controlId= "destination">
											<Form.Label>Destination</Form.Label>
														<Form.Control 
							 								type = 'text'
							 								value = {newDestination}
							 								onChange = {e => setNewDestination(e.target.value)}
							 								required
														/>
										</Form.Group>

										<Row>
			 								<Form.Group as={Col} controlId= "dateDeparture">
			 									<Form.Label>Departure Date</Form.Label>
														<Form.Control 
							 								type = 'date'
							 								value = {newDateDeparture}
							 								onChange = {e => setNewDateDeparture(e.target.value)}
							 								required
														/>
													</Form.Group>

											<Form.Group as={Col} controlId= "dateArrival">
													 <Form.Label>Arrival Date</Form.Label>
														<Form.Control 
							 								type = 'date'
							 								value = {newDateArrival}
							 								onChange = {e => setNewDateArrival(e.target.value)}
							 								
														/>
													</Form.Group>
										</Row>

										<Row className="mb-3">
										    <Form.Group as={Col} controlId="adultpax">
										      <Form.Label>Adult</Form.Label>
										      <Form.Control 
										      	type="number" 
										      	placeholder="1" 
										      	value = {newAdult}
										      	onChange = {e => setNewAdult(e.target.value)}
										      />
										    </Form.Group>

										    <Form.Group as={Col} controlId="childpax">
										      <Form.Label>Child</Form.Label>
										      <Form.Control 
										      	type="number" 
										      	placeholder="0" 
										      	value = {newChild}
										      	onChange = {e => setNewChild(e.target.value)}
										      	/>
										    </Form.Group>

										     <Form.Group as={Col} controlId="infant">
										      <Form.Label>Infant</Form.Label>
										      <Form.Control 
										      	type="number" 
										      	placeholder="0" 
										      	value = {newInfant}
										      	onChange = {e => setNewInfant(e.target.value)}

										      	/>
										    </Form.Group>
										  </Row> 						
							</Modal.Body>

							 <Modal.Footer>
					            <Button variant="secondary" type="submit">Submit</Button>
					        </Modal.Footer>

						</Form>
						</Modal>

			</Container>
		</Fragment>
	)
}